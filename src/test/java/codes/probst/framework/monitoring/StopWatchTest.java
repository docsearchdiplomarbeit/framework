package codes.probst.framework.monitoring;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Test;

import codes.probst.framework.monitoring.StopWatch;

public class StopWatchTest {
	
	@Test
	public void testElapsedTime() {
		
		StopWatch watch = new StopWatch() {
			@Override
			protected long getCurrentTime() {
				return 11_000_000l;
			}
		};

		setStartTime(watch, 10_000_000l);
		
		Assert.assertEquals(1, watch.getElapsedTime());
		setEndTime(watch, 15_000_000l);
		Assert.assertEquals(5, watch.getElapsedTime());
	}
	
	@Test
	public void testToString() {
		StopWatch watch = new StopWatch();
		setStartTime(watch, 10_000_000l);
		setEndTime(watch, 11_000_000l);
		
		Assert.assertEquals("StopWatch [startTime=10000000, endTime=11000000, elapsedTime=1]", watch.toString());
	}
	
	@Test
	public void testEquals() {
		StopWatch watch1 = new StopWatch();
		StopWatch watch2 = new StopWatch();
		StopWatch watch3 = new StopWatch();
		StopWatch watch4 = new StopWatch();
		
		setStartTime(watch1, 1000l);
		setStartTime(watch2, 1000l);
		setStartTime(watch3, 2000l);
		setStartTime(watch4, 2000l);

		
		setEndTime(watch1, 1100l);
		setEndTime(watch2, 1100l);
		setEndTime(watch3, 2100l);
		setEndTime(watch4, 1100l);
		
		Assert.assertTrue(watch1.equals(watch1));
		Assert.assertTrue(watch1.equals(watch2));
		Assert.assertFalse(watch1.equals(null));
		Assert.assertFalse(watch1.equals("jUnit"));
		Assert.assertFalse(watch1.equals(watch3));
		Assert.assertFalse(watch1.equals(watch4));
	}
	
	@Test
	public void testHashCode() throws InterruptedException {
		StopWatch watch = new StopWatch();
		watch.start();
		watch.stop();

		StopWatch watch2 = new StopWatch();
		watch2.start();
		Thread.sleep(100l);
		watch2.stop();
		
		Assert.assertEquals(watch.hashCode(), watch.hashCode());
		Assert.assertNotEquals(watch.hashCode(), watch2.hashCode());
	}
	
	@Test(expected = IllegalStateException.class)
	public void testStartStarted() {
		StopWatch watch = new StopWatch();
		watch.start();
		watch.start();
	}
	
	@Test(expected = IllegalStateException.class)
	public void testStopStoped() {
		StopWatch watch = new StopWatch();
		watch.start();
		watch.stop();
		watch.stop();
	}
	
	@Test(expected = IllegalStateException.class)
	public void testStopNotStarted() {
		StopWatch watch = new StopWatch();
		watch.stop();
	}

	@Test
	public void testUnitConversion() {
		StopWatch watch = new StopWatch();
		watch.start();
		try {
			Thread.sleep(1000l);
		} catch (InterruptedException e) {
		}
		watch.stop();
		
		long durationInNanos = watch.getElapsedTime(TimeUnit.NANOSECONDS);
		long durationInMicros = watch.getElapsedTime(TimeUnit.MICROSECONDS);
		long durationInMillis = watch.getElapsedTime(TimeUnit.MILLISECONDS);
		long durationInSeconds = watch.getElapsedTime(TimeUnit.SECONDS);
		
		Assert.assertEquals(durationInNanos / 1000 * 1000, TimeUnit.MICROSECONDS.toNanos(durationInMicros));
		Assert.assertEquals(durationInNanos / 1000000 * 1000000, TimeUnit.MILLISECONDS.toNanos(durationInMillis));
		Assert.assertEquals(durationInNanos / 1000000000 * 1000000000, TimeUnit.SECONDS.toNanos(durationInSeconds));
	}
	
	@Test
	public void testCalculation() {
		long start = 1000000000000000l;
		long stop = 1000000000001000l;
		long duration = stop - start;
		
		StopWatch watch = new StopWatch() {
			private List<Long> values = new ArrayList<>(Arrays.asList(start, stop));
			
			@Override
			protected long getCurrentTime() {
				return values.remove(0);
			}
		};
		
		watch.start();
		long watchDuration = watch.stop(TimeUnit.NANOSECONDS);
		Assert.assertEquals(duration, watchDuration);
	}
	
	private static void setStartTime(StopWatch watch, Long startTime) {
		setValue(watch, "startTime", startTime);
	}
	private static void setEndTime(StopWatch watch, Long endTime) {
		setValue(watch, "endTime", endTime);
	}
	
	private static void setValue(Object object, String fieldName, Object value) {
		try {
			Field field = null;
			try {
				field = object.getClass().getDeclaredField(fieldName);
			} catch(NoSuchFieldException e) {
				field = object.getClass().getSuperclass().getDeclaredField(fieldName);
			}
			boolean accessible = field.isAccessible();
			field.setAccessible(true);
			field.set(object, value);
			field.setAccessible(accessible);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}
}
