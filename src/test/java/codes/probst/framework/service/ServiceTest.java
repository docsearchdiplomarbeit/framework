package codes.probst.framework.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.framework.dao.Dao;
import codes.probst.framework.dao.MockDao;
import codes.probst.framework.pagination.Pageable;

public class ServiceTest {
	private Service<Object, Object> service;
	private MockDao<Object, Object> dao;
	
	@Before
	public void setup() {
		AbstractService<Dao<Object, Object>, Object, Object> service = new AbstractService<Dao<Object, Object>, Object, Object>() {
			
			@Override
			protected boolean isNew(Object object) {
				return "NEW".equals(object);
			}
			
		};
		
		dao = new MockDao<Object, Object>() {
			
			@Override
			public Optional<Object> findByKey(Object key) {
				return getCollection().stream().filter(e -> e.equals(key)).findAny();
			}
			
		};
		
		service.setDao(dao);
		
		this.service = service;
	}
	
	@Test
	public void testSaveNew() {
		service.save("NEW");
		
		Assert.assertEquals(Arrays.asList("NEW"), dao.getInserted());
		Assert.assertEquals(Arrays.asList(), dao.getDeleted());
		Assert.assertEquals(Arrays.asList(), dao.getUpdated());
	}
	
	@Test
	public void testSaveUpdated() {
		service.save("UPDATED");
		
		Assert.assertEquals(Arrays.asList(), dao.getInserted());
		Assert.assertEquals(Arrays.asList(), dao.getDeleted());
		Assert.assertEquals(Arrays.asList("UPDATED"), dao.getUpdated());
	}
	
	@Test
	public void testRemove() {
		service.remove("REMOVED");
		
		Assert.assertEquals(Arrays.asList(), dao.getInserted());
		Assert.assertEquals(Arrays.asList("REMOVED"), dao.getDeleted());
		Assert.assertEquals(Arrays.asList(), dao.getUpdated());
	}

	
	@Test
	public void testGetById() {
		dao.setCollection(Arrays.asList("1", "2", "3", "4"));
		
		Assert.assertEquals("2", service.getByKey("2").get());
		Assert.assertEquals("4", service.getByKey("4").get());
	}

	
	@Test
	public void testGetByNonExistingId() {
		dao.setCollection(Arrays.asList("1", "2", "3", "4"));
		
		Assert.assertFalse(service.getByKey("non existing").isPresent());
	}
	
	@Test
	public void testGetAllPage0() {
		dao.setCollection(Arrays.asList("1", "2", "3", "4"));
		
		Pageable pageable = new Pageable();
		pageable.setCurrentPage(0);
		pageable.setPageSize(2);
		
		Collection<Object> collection = service.getAll(pageable);
		Assert.assertNotNull(collection);
		Assert.assertEquals(2, collection.size());
		Assert.assertEquals(Arrays.asList("1", "2"), collection);
	}
	
	@Test
	public void testGetAllPage1() {
		dao.setCollection(Arrays.asList("1", "2", "3", "4"));
		
		Pageable pageable = new Pageable();
		pageable.setCurrentPage(1);
		pageable.setPageSize(2);
		
		Collection<Object> collection = service.getAll(pageable);
		Assert.assertNotNull(collection);
		Assert.assertEquals(2, collection.size());
		Assert.assertEquals(Arrays.asList("3", "4"), collection);
	}
}
