package codes.probst.framework.file.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FileServiceTest {
	private FileService fileService;

	@Before
	public void setup() {
		fileService = new DefaultFileService();
	}
	
	@Test
	public void testZipExtraction() throws IOException {
		String randomName = UUID.randomUUID().toString();
		Path folder = Paths.get(System.getProperty("java.io.tmpdir")).resolve(randomName);
		
		try(ZipInputStream zis = new ZipInputStream(getClass().getResourceAsStream("/codes/probst/framework/file/test.zip"))) {
			fileService.extractZipToFolder(zis, folder);
			
			Assert.assertTrue(Files.exists(folder.resolve("file")));
			Assert.assertTrue(Files.exists(folder.resolve("folder")));
			Assert.assertTrue(Files.isDirectory(folder.resolve("folder")));
		}
		
		//call gc to prevent windows bug see http://bugs.java.com/bugdatabase/view_bug.do?bug_id=4715154
		System.gc();
		FileUtils.deleteDirectory(folder.toFile());
	}
	
	@Test(expected = IOException.class)
	public void getFromInputStreamWithException() throws IOException {
		fileService.getBytesFromInputStream(new InputStream() {
			
			@Override
			public int read() throws IOException {
				throw new IOException();
			}
		});
	}
	
	@Test(expected = IOException.class)
	public void readFromNonReachableUrl() throws MalformedURLException, IOException {
		fileService.getBytesFromUrl(new URL("http://example.donotexist/"));
	}
	
	@Test
	public void readFromEmptyInputStream() throws IOException {
		InputStream is = new ByteArrayInputStream(new byte[0]);
		byte[] bytes = fileService.getBytesFromInputStream(is);
		
		Assert.assertNotNull(bytes);
		Assert.assertEquals(0, bytes.length);
	}
	
	@Test
	public void readFromInputStream() throws IOException {
		byte[] givenBytes = new byte[] { 1, 4, 5, 2, 7, 9, 2 };
		InputStream is = new ByteArrayInputStream(givenBytes);
		byte[] bytes = fileService.getBytesFromInputStream(is);
		
		Assert.assertNotNull(bytes);
		Assert.assertEquals(givenBytes.length, bytes.length);
		Assert.assertArrayEquals(givenBytes, bytes);
	}
	
	@Test
	public void readFromUrl() throws IOException {
		URL url = getClass().getResource("/codes/probst/framework/file/test");
		byte[] bytes = fileService.getBytesFromUrl(url);

		Assert.assertNotNull(bytes);
		Assert.assertEquals(20, bytes.length);
		Assert.assertArrayEquals(new byte[] {
			106, 85, 110, 105, 116, 32, 84, 101, 115, 116, 32, 67, 97, 115, 101, 32, 68, 97, 116, 97
		}, bytes);
	}
}
