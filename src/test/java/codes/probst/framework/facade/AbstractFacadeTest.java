package codes.probst.framework.facade;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.framework.populator.DefaultConfigurablePopulator;
import codes.probst.framework.populator.PopulationOption;
import codes.probst.framework.populator.Populator;
import codes.probst.framework.service.Service;

public class AbstractFacadeTest {
	private AbstractFacade<Source, Target, Service<Source, Long>, Long> facade;
	

	@Before
	public void setup() {
		Map<PopulationOption, Collection<Populator<Source, Target>>> populators = new HashMap<>();
		populators.put(TestPopulationOption.OPTION_1, Arrays.asList(new Populator1()));
		populators.put(TestPopulationOption.OPTION_2, Arrays.asList(new Populator2()));
		
		
		DefaultConfigurablePopulator<Source, Target> populator = new DefaultConfigurablePopulator<>();
		
		populator.setPopulators(populators);
		
		AbstractFacade<Source, Target, Service<Source, Long>, Long> facade = new AbstractFacade<Source, Target, Service<Source, Long>, Long>() {
			
		};
		facade.setConverter(value -> new Target());		
		facade.setPopulator(populator);
		this.facade = facade;
	}
	
	@Test
	public void testConversion() {
		Target target = facade.convert(new Source());
		Assert.assertNotNull(target);
	}
	
	@Test
	public void testNullConversion() {
		Target target = facade.convert(null);
		Assert.assertNull(target);
	}
	
	@Test
	public void testConvertAndPopulate() {
		Source source = new Source();
		source.setField1("FIELD_1");
		source.setField2("FIELD_2");
		
		Target target = facade.convertAndPopulate(source, TestPopulationOption.OPTION_1);
		Assert.assertNotNull(target);
		Assert.assertEquals("FIELD_1", target.getField1());
		Assert.assertNull(target.getField2());
	}
	
	@Test
	public void testConvertAndPopulateNullSource() {
		Target target = facade.convertAndPopulate(null, TestPopulationOption.OPTION_1);
		Assert.assertNull(target);
	}
	
	@Test
	public void testConvertAndPopulateAll() {

		Source source1 = new Source();
		source1.setField1("FIELD_1.1");
		source1.setField2("FIELD_2.1");


		Source source2 = new Source();
		source2.setField1("FIELD_1.2");
		source2.setField2("FIELD_2.2");
		Collection<Target> targets = facade.convertAndPopulateAll(Arrays.asList(source1, source2), TestPopulationOption.OPTION_1);
		Assert.assertNotNull(targets);
		Assert.assertEquals(2, targets.size());
		
		Iterator<Target> it = targets.iterator();
		{
			Target target = it.next();
			Assert.assertEquals("FIELD_1.1", target.getField1());
			Assert.assertNull(target.getField2());
		}

		{
			Target target = it.next();
			Assert.assertEquals("FIELD_1.2", target.getField1());
			Assert.assertNull(target.getField2());
		}
	}
	
	
	private static class Populator1 implements Populator<Source, Target> {
		@Override
		public void populate(Source source, Target target) {
			target.setField1(source.getField1());
		}
	}
	
	private static class Populator2 implements Populator<Source, Target> {
		@Override
		public void populate(Source source, Target target) {
			target.setField2(source.getField2());
		}
	}
	
	
	private static class Source {
		private String field1;
		private String field2;
		
		public String getField1() {
			return field1;
		}
		public String getField2() {
			return field2;
		}
		public void setField1(String field1) {
			this.field1 = field1;
		}
		public void setField2(String field2) {
			this.field2 = field2;
		}
	}
	
	private static class Target {
		private String field1;
		private String field2;
		
		public String getField1() {
			return field1;
		}
		public String getField2() {
			return field2;
		}
		public void setField1(String field1) {
			this.field1 = field1;
		}
		public void setField2(String field2) {
			this.field2 = field2;
		}
		
	}
	
	private static class TestPopulationOption extends PopulationOption {
		static TestPopulationOption OPTION_1 = new TestPopulationOption("1", 100);
		static TestPopulationOption OPTION_2 = new TestPopulationOption("2", 200);
		
		public TestPopulationOption(String code, int order) {
			super(code, order);
		}
		
	}
}
