package codes.probst.framework.dao;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "Value")
public class Value {
	@Id
	private Long id;
	private String value;
	
	public Value() {
	}
	
	public Value(Long id, String value) {
		this.id = id;
		this.value = value;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}
