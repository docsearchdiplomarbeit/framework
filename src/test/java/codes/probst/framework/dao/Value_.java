package codes.probst.framework.dao;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Value.class)
public class Value_ {
    public static volatile SingularAttribute<Value, Long> id;
    public static volatile SingularAttribute<Value, String> value;
}