package codes.probst.framework.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import codes.probst.framework.pagination.Pageable;

public abstract class MockDao<T, K> implements Dao<T, K> {
	private List<T> collection;
	private List<T> updated;
	private List<T> inserted;
	private List<K> deleted;
	
	public MockDao() {
		updated = new ArrayList<>();
		inserted = new ArrayList<>();
		deleted = new ArrayList<>();
	}
	
	@Override
	public long count() {
		return collection.size();
	}
	
	@Override
	public Collection<T> findAll(Pageable pageable) {
		int start = pageable.getCurrentPage() * pageable.getPageSize();
		int end = start + pageable.getPageSize();
		
		return collection.subList(start, end);
	}

	@Override
	public void update(T value) {
		updated.add(value);
	}

	@Override
	public void insert(T value) {
		inserted.add(value);
	}

	@Override
	public void delete(K key) {
		deleted.add(key);
	}


	public List<K> getDeleted() {
		return deleted;
	}
	
	public List<T> getInserted() {
		return inserted;
	}
	
	public List<T> getUpdated() {
		return updated;
	}
	
	public void setCollection(List<T> collection) {
		this.collection = collection;
	}
	
	public List<T> getCollection() {
		return collection;
	}

	
}
