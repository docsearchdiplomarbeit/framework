package codes.probst.framework.dao;

import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;
import codes.probst.framework.pagination.SortDirection;

public class JpaDaoTest {
	private static final Sort[] DEFAULT_SORT = new Sort[1];
	static {
		DEFAULT_SORT[0] = new Sort();
		DEFAULT_SORT[0].setName("id");
		DEFAULT_SORT[0].setDirection(SortDirection.valueOf("ASCENDING"));
	}
	
	
	private Dao<Value, Long> dao;
	private EntityManager manager;
	
	@Before
	public void setup() {
		manager = Persistence.createEntityManagerFactory("junit").createEntityManager();
		
		AbstractJpaDao<Value, Long> dao = new AbstractJpaDao<Value, Long>(Value.class, Value_.id) {
			
		};
		dao.setEntityManager(manager);
		
		this.dao = dao;
		startTransaction();
	}

	public void startTransaction()  {
		manager.getTransaction().begin();
	}
	
	@After
	public void rollbackTransaction() {
		manager.getTransaction().rollback();
	}
	
	@Test
	public void testFindAllPage1() {
		dao.insert(new Value(Long.valueOf(0l), "Value 1"));
		dao.insert(new Value(Long.valueOf(5l), "Value 5"));
		dao.insert(new Value(Long.valueOf(2l), "Value 2"));
		dao.insert(new Value(Long.valueOf(6l), "Value 6"));
		dao.insert(new Value(Long.valueOf(3l), "Value 3"));
		dao.insert(new Value(Long.valueOf(7l), "Value 7"));
		dao.insert(new Value(Long.valueOf(4l), "Value 4"));
		dao.insert(new Value(Long.valueOf(8l), "Value 8"));

		Pageable pageable = new Pageable();
		pageable.setCurrentPage(1);
		pageable.setPageSize(2);
		pageable.setSort(DEFAULT_SORT);
		
		Collection<Value> values = dao.findAll(pageable);
		
		Assert.assertNotNull(values);
		Assert.assertEquals(2, values.size());
		
		Iterator<Value> it = values.iterator();
		{
			Value value = it.next();
			Assert.assertEquals(Long.valueOf(3l), value.getId());
			Assert.assertEquals("Value 3", value.getValue());
		}
		{
			Value value = it.next();
			Assert.assertEquals(Long.valueOf(4l), value.getId());
			Assert.assertEquals("Value 4", value.getValue());
		}
		
	}
	
	@Test
	public void testFindAllPage0() {
		dao.insert(new Value(Long.valueOf(0l), "Value 1"));
		dao.insert(new Value(Long.valueOf(5l), "Value 5"));
		dao.insert(new Value(Long.valueOf(2l), "Value 2"));
		dao.insert(new Value(Long.valueOf(6l), "Value 6"));
		dao.insert(new Value(Long.valueOf(3l), "Value 3"));
		dao.insert(new Value(Long.valueOf(7l), "Value 7"));
		dao.insert(new Value(Long.valueOf(4l), "Value 4"));
		dao.insert(new Value(Long.valueOf(8l), "Value 8"));

		Pageable pageable = new Pageable();
		pageable.setCurrentPage(0);
		pageable.setPageSize(2);
		pageable.setSort(DEFAULT_SORT);
		
		Collection<Value> values = dao.findAll(pageable);
		
		Assert.assertNotNull(values);
		Assert.assertEquals(2, values.size());
		
		Iterator<Value> it = values.iterator();
		{
			Value value = it.next();
			Assert.assertEquals(Long.valueOf(0l), value.getId());
			Assert.assertEquals("Value 1", value.getValue());
		}
		{
			Value value = it.next();
			Assert.assertEquals(Long.valueOf(2l), value.getId());
			Assert.assertEquals("Value 2", value.getValue());
		}
		
	}
	
	
	
	@Test
	public void testDelete() {
		Value value = new Value();
		value.setId(Long.valueOf(0l));
		value.setValue("VALUE");
		
		dao.insert(value);
		dao.delete(Long.valueOf(0l));
		
		Optional<Value> optionalStoredValue = dao.findByKey(Long.valueOf(0l));
		
		Assert.assertFalse(optionalStoredValue.isPresent());
	}
	
	@Test
	public void testUpdate() {
		Value value = new Value();
		value.setId(Long.valueOf(0l));
		value.setValue("VALUE");
		
		dao.insert(value);
		value.setValue("JUNIT");
		dao.update(value);
		
		Optional<Value> optionalStoredValue = dao.findByKey(Long.valueOf(0l));
		
		Assert.assertTrue(optionalStoredValue.isPresent());
		
		Value storedValue = optionalStoredValue.get();
		
		Assert.assertEquals(Long.valueOf(0l), storedValue.getId());
		Assert.assertEquals("JUNIT", storedValue.getValue());
	}
	
	@Test
	public void testInsert() {
		Value value = new Value();
		value.setId(Long.valueOf(0l));
		value.setValue("VALUE");
		
		dao.insert(value);
		
		Optional<Value> optionalStoredValue = dao.findByKey(Long.valueOf(0l));
		
		Assert.assertTrue(optionalStoredValue.isPresent());
		
		Value storedValue = optionalStoredValue.get();
		
		Assert.assertEquals(Long.valueOf(0l), storedValue.getId());
		Assert.assertEquals("VALUE", storedValue.getValue());
	}
	
}
