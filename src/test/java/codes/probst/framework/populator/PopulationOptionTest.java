package codes.probst.framework.populator;

import org.junit.Assert;
import org.junit.Test;

public class PopulationOptionTest {
	private static final PopulationOption OPTION_1 = new TestPopulationOption("OPTION", 1);
	private static final PopulationOption OPTION_2 = new TestPopulationOption("OPTION", 2);
	private static final PopulationOption OPTION_3 = new TestPopulationOption("OPTION_3", 1);
	private static final PopulationOption OPTION_NULL = new TestPopulationOption(null, 1);

	@Test
	public void testPopulation() {
		String code = "jUnit";
		int order = 123;
		
		PopulationOption option = new TestPopulationOption(code, order);
		
		Assert.assertEquals(code, option.getCode());
		Assert.assertEquals(order, option.getOrder());
	}
	
	@Test
	public void testCompareTo() {
		Assert.assertEquals(-1, OPTION_1.compareTo(OPTION_2));
		Assert.assertEquals(0, OPTION_1.compareTo(OPTION_1));
		Assert.assertEquals(1, OPTION_2.compareTo(OPTION_1));
	}
	
	@Test
	public void testHashCode() {
		Assert.assertEquals(OPTION_1.hashCode(), new TestPopulationOption("OPTION", 1).hashCode());
		Assert.assertEquals(OPTION_NULL.hashCode(), new TestPopulationOption(null, 1).hashCode());
		
		Assert.assertNotEquals(OPTION_1.hashCode(), OPTION_2.hashCode());
		Assert.assertNotEquals(OPTION_1.hashCode(), OPTION_3.hashCode());
	}
	
	@Test
	public void testEquals() {
		Assert.assertFalse(OPTION_1.equals("jUnit"));
		Assert.assertFalse(OPTION_1.equals(OPTION_2));
		Assert.assertFalse(OPTION_2.equals(OPTION_1));
		Assert.assertFalse(OPTION_1.equals(OPTION_3));
		Assert.assertFalse(OPTION_1.equals(null));
		Assert.assertFalse(OPTION_NULL.equals(OPTION_1));
		
		Assert.assertTrue(OPTION_1.equals(OPTION_1));
		Assert.assertTrue(OPTION_1.equals(new TestPopulationOption("OPTION", 1)));
		Assert.assertTrue(OPTION_NULL.equals(new TestPopulationOption(null, 1)));
	}
	
	
	public static class TestPopulationOption extends PopulationOption {
		
		public TestPopulationOption(String code, int order) {
			super(code, order);
		}
	}
}
