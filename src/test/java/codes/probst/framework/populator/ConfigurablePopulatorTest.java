package codes.probst.framework.populator;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class ConfigurablePopulatorTest {
	private ConfigurablePopulator<Source, Target> populator;
	
	
	@Before
	public void setup() {
		Map<PopulationOption, Collection<Populator<Source, Target>>> populators = new HashMap<>();
		populators.put(TestPopulationOption.OPTION_1, Arrays.asList(new Populator1()));
		populators.put(TestPopulationOption.OPTION_2, Arrays.asList(new Populator2()));
		populators.put(TestPopulationOption.OPTION_3, Arrays.asList(new Populator3()));
		populators.put(TestPopulationOption.OPTION_1_2, Arrays.asList(new Populator1(), new Populator2()));
		populators.put(TestPopulationOption.OPTION_EMPTY, Collections.emptyList());
		
		
		DefaultConfigurablePopulator<Source, Target> populator = new DefaultConfigurablePopulator<>();
		
		populator.setPopulators(populators);
		
		this.populator = populator;
	}
	
	@Test
	public void testNullPopulatorList() {
		populator.populate(new Source(), new ExceptionTarget(), TestPopulationOption.OPTION_NULL);
	}
	
	@Test
	public void testEmptyPopulatorList() {
		populator.populate(new Source(), new ExceptionTarget(), TestPopulationOption.OPTION_EMPTY);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNullTarget() {
		populator.populate(new Source(), null, TestPopulationOption.OPTION_1);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNullSource() {
		populator.populate(null, new Target(), TestPopulationOption.OPTION_1);
	}
	
	@Test
	public void testEmptyOptions() {
		Source source = createSource();
		Target target = new Target();
		populator.populate(source, target);
		
		Assert.assertNull(target.getField1());
		Assert.assertNull(target.getField2());
	}

	
	@Test
	public void testPopulator1() {
		Source source = createSource();
		Target target = new Target();
		populator.populate(source, target, TestPopulationOption.OPTION_1);
		
		Assert.assertEquals("FIELD_1", target.getField1());
		Assert.assertNull(target.getField2());
	}
	
	@Test
	public void testPopulator1InOrder() {
		Source source = createSource();
		Target target = new Target();
		populator.populate(source, target, TestPopulationOption.OPTION_3, TestPopulationOption.OPTION_1, TestPopulationOption.OPTION_2);
		
		Assert.assertEquals("FIELD_1", target.getField1());
		Assert.assertEquals("FIELD_2", target.getField2());
	}
	
	@Test
	public void testPopulator1OutOfOrder() {
		Source source = createSource();
		Target target = new Target();
		populator.populate(source, target, TestPopulationOption.OPTION_2, TestPopulationOption.OPTION_1, TestPopulationOption.OPTION_3);
		
		Assert.assertEquals("FIELD_1", target.getField1());
		Assert.assertEquals("FIELD_2", target.getField2());
	}
	
	@Test
	public void testOptionWithMultiplePopulators() {
		Source source = createSource();
		Target target = new Target();
		populator.populate(source, target, TestPopulationOption.OPTION_1_2);
		
		Assert.assertEquals("FIELD_1", target.getField1());
		Assert.assertEquals("FIELD_2", target.getField2());
	}
	
	@Test
	public void testPopulator2() {
		Source source = createSource();
		Target target = new Target();
		populator.populate(source, target, TestPopulationOption.OPTION_1);
		
		Assert.assertEquals("FIELD_1", target.getField1());
		Assert.assertNull(target.getField2());
	}
	
	
	private Source createSource() {
		Source source = new Source();
		
		source.setField1("FIELD_1");
		source.setField2("FIELD_2");
		
		return source;
	}
	
	private static class Populator1 implements Populator<Source, Target> {
		@Override
		public void populate(Source source, Target target) {
			target.setField1(source.getField1());
		}
	}
	
	private static class Populator2 implements Populator<Source, Target> {
		@Override
		public void populate(Source source, Target target) {
			target.setField2(source.getField2());
		}
	}
	
	private static class Populator3 implements Populator<Source, Target> {
		@Override
		public void populate(Source source, Target target) {
			target.setField1("TEST_" + source.getField1());
			target.setField2("TEST_" + source.getField2());
		}
	}
	
	private static class Source {
		private String field1;
		private String field2;
		
		public String getField1() {
			return field1;
		}
		public String getField2() {
			return field2;
		}
		public void setField1(String field1) {
			this.field1 = field1;
		}
		public void setField2(String field2) {
			this.field2 = field2;
		}
	}
	
	private static class Target {
		private String field1;
		private String field2;
		
		public String getField1() {
			return field1;
		}
		public String getField2() {
			return field2;
		}
		public void setField1(String field1) {
			this.field1 = field1;
		}
		public void setField2(String field2) {
			this.field2 = field2;
		}
		
	}
	
	private static class ExceptionTarget extends Target {
		
		public String getField1() {
			throw new RuntimeException();
		}
		public String getField2() {
			throw new RuntimeException();
		}
		public void setField1(String field1) {
			throw new RuntimeException();
		}
		public void setField2(String field2) {
			throw new RuntimeException();
		}
		
	}
	
	private static class TestPopulationOption extends PopulationOption {
		static TestPopulationOption OPTION_1 = new TestPopulationOption("1", 100);
		static TestPopulationOption OPTION_2 = new TestPopulationOption("2", 200);
		static TestPopulationOption OPTION_3 = new TestPopulationOption("3", 1);
		static TestPopulationOption OPTION_1_2 = new TestPopulationOption("1+2", 1);
		static TestPopulationOption OPTION_NULL = new TestPopulationOption("NULL", 1);
		static TestPopulationOption OPTION_EMPTY = new TestPopulationOption("EMPTY", 1);
		
		public TestPopulationOption(String code, int order) {
			super(code, order);
		}
		
	}
}
