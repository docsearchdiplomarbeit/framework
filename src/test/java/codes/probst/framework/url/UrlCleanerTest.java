package codes.probst.framework.url;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UrlCleanerTest {
	private UrlCleaner urlCleaner;

	@Before
	public void setup() {
		urlCleaner = new DefaultUrlCleaner();
	}

	
	@Test
	public void testEmpty() {
		Assert.assertEquals("", urlCleaner.clean(""));
	}
	
	@Test
	public void testNull() {
		Assert.assertNull(urlCleaner.clean(null));
	}
	
	@Test
	public void testUnknownChar() {
		Assert.assertEquals("/-", urlCleaner.clean("/\uFFFF"));
	}
	
	@Test
	public void testNumbers() {
		Assert.assertEquals("/0.html", urlCleaner.clean("/0.html"));
		Assert.assertEquals("/1.html", urlCleaner.clean("/1.html"));
		Assert.assertEquals("/2.html", urlCleaner.clean("/2.html"));
		Assert.assertEquals("/3.html", urlCleaner.clean("/3.html"));
		Assert.assertEquals("/4.html", urlCleaner.clean("/4.html"));
		Assert.assertEquals("/5.html", urlCleaner.clean("/5.html"));
		Assert.assertEquals("/6.html", urlCleaner.clean("/6.html"));
		Assert.assertEquals("/7.html", urlCleaner.clean("/7.html"));
		Assert.assertEquals("/8.html", urlCleaner.clean("/8.html"));
		Assert.assertEquals("/9.html", urlCleaner.clean("/9.html"));
	}

	@Test
	public void testReservedCharacters() {
		Assert.assertEquals("/-.html", urlCleaner.clean("/:.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/?.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/#.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/[.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/].html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/@.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/!.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/$.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/'.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/(.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/).html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/*.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/+.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/,.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/;.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/=.html"));
	}

	@Test
	public void testSpace() {
		Assert.assertEquals("/-.html", urlCleaner.clean("/ .html"));
	}

	@Test
	public void testSpecialCharsOf_() {
		Assert.assertEquals("/_", urlCleaner.clean("/\uFF3F"));
	}

	@Test
	public void testSpecialCharsOf0() {
		Assert.assertEquals("/0", urlCleaner.clean("/\u2070"));
		Assert.assertEquals("/0", urlCleaner.clean("/\u2080"));
		Assert.assertEquals("/0", urlCleaner.clean("/\uFF10"));
		Assert.assertEquals("/0", urlCleaner.clean("/\u24EA"));
		Assert.assertEquals("/0", urlCleaner.clean("/\u24FF"));
		Assert.assertEquals("/0", urlCleaner.clean("/\uFF10"));
	}
	
	@Test
	public void testSpecialCharsOf1() {
		Assert.assertEquals("/1", urlCleaner.clean("/\u00B9"));
		Assert.assertEquals("/1", urlCleaner.clean("/\u2081"));
		Assert.assertEquals("/1", urlCleaner.clean("/\u2460"));
		Assert.assertEquals("/1", urlCleaner.clean("/\u24F5"));
		Assert.assertEquals("/1", urlCleaner.clean("/\u2776"));
		Assert.assertEquals("/1", urlCleaner.clean("/\u2780"));
		Assert.assertEquals("/1", urlCleaner.clean("/\u278A"));
		Assert.assertEquals("/1", urlCleaner.clean("/\uFF11"));
		Assert.assertEquals("/1-", urlCleaner.clean("/\u2488"));
	}

	@Test
	public void testSpecialCharsOf2() {
		Assert.assertEquals("/2", urlCleaner.clean("/\u00B2"));
		Assert.assertEquals("/2", urlCleaner.clean("/\u2082"));
		Assert.assertEquals("/2", urlCleaner.clean("/\u2461"));
		Assert.assertEquals("/2", urlCleaner.clean("/\u24F6"));
		Assert.assertEquals("/2", urlCleaner.clean("/\u2777"));
		Assert.assertEquals("/2", urlCleaner.clean("/\u2781"));
		Assert.assertEquals("/2", urlCleaner.clean("/\u278B"));
		Assert.assertEquals("/2", urlCleaner.clean("/\uFF12"));
		Assert.assertEquals("/2-", urlCleaner.clean("/\u2489"));
	}

	@Test
	public void testSpecialCharsOf3() {
		Assert.assertEquals("/3", urlCleaner.clean("/\u00B3"));
		Assert.assertEquals("/3", urlCleaner.clean("/\u2083"));
		Assert.assertEquals("/3", urlCleaner.clean("/\u2462"));
		Assert.assertEquals("/3", urlCleaner.clean("/\u24F7"));
		Assert.assertEquals("/3", urlCleaner.clean("/\u2778"));
		Assert.assertEquals("/3", urlCleaner.clean("/\u2782"));
		Assert.assertEquals("/3", urlCleaner.clean("/\u278C"));
		Assert.assertEquals("/3", urlCleaner.clean("/\uFF13"));
		Assert.assertEquals("/3-", urlCleaner.clean("/\u248A"));
	}

	@Test
	public void testSpecialCharsOf4() {
		Assert.assertEquals("/4", urlCleaner.clean("/\u2074"));
		Assert.assertEquals("/4", urlCleaner.clean("/\u2084"));
		Assert.assertEquals("/4", urlCleaner.clean("/\u2463"));
		Assert.assertEquals("/4", urlCleaner.clean("/\u24F8"));
		Assert.assertEquals("/4", urlCleaner.clean("/\u2779"));
		Assert.assertEquals("/4", urlCleaner.clean("/\u2783"));
		Assert.assertEquals("/4", urlCleaner.clean("/\u278D"));
		Assert.assertEquals("/4", urlCleaner.clean("/\uFF14"));
		Assert.assertEquals("/4-", urlCleaner.clean("/\u248B"));
	}

	@Test
	public void testSpecialCharsOf5() {
		Assert.assertEquals("/5", urlCleaner.clean("/\u2075"));
		Assert.assertEquals("/5", urlCleaner.clean("/\u2085"));
		Assert.assertEquals("/5", urlCleaner.clean("/\u2464"));
		Assert.assertEquals("/5", urlCleaner.clean("/\u24F9"));
		Assert.assertEquals("/5", urlCleaner.clean("/\u277A"));
		Assert.assertEquals("/5", urlCleaner.clean("/\u2784"));
		Assert.assertEquals("/5", urlCleaner.clean("/\u278E"));
		Assert.assertEquals("/5", urlCleaner.clean("/\uFF15"));
		Assert.assertEquals("/5-", urlCleaner.clean("/\u248C"));
	}

	@Test
	public void testSpecialCharsOf6() {
		Assert.assertEquals("/6", urlCleaner.clean("/\u2076"));
		Assert.assertEquals("/6", urlCleaner.clean("/\u2086"));
		Assert.assertEquals("/6", urlCleaner.clean("/\u2465"));
		Assert.assertEquals("/6", urlCleaner.clean("/\u24FA"));
		Assert.assertEquals("/6", urlCleaner.clean("/\u277B"));
		Assert.assertEquals("/6", urlCleaner.clean("/\u2785"));
		Assert.assertEquals("/6", urlCleaner.clean("/\u278F"));
		Assert.assertEquals("/6", urlCleaner.clean("/\uFF16"));
		Assert.assertEquals("/6-", urlCleaner.clean("/\u248D"));
	}

	@Test
	public void testSpecialCharsOf7() {
		Assert.assertEquals("/7", urlCleaner.clean("/\u2077"));
		Assert.assertEquals("/7", urlCleaner.clean("/\u2087"));
		Assert.assertEquals("/7", urlCleaner.clean("/\u2466"));
		Assert.assertEquals("/7", urlCleaner.clean("/\u24FB"));
		Assert.assertEquals("/7", urlCleaner.clean("/\u277C"));
		Assert.assertEquals("/7", urlCleaner.clean("/\u2786"));
		Assert.assertEquals("/7", urlCleaner.clean("/\u2790"));
		Assert.assertEquals("/7", urlCleaner.clean("/\uFF17"));
		Assert.assertEquals("/7-", urlCleaner.clean("/\u248E"));
	}

	@Test
	public void testSpecialCharsOf8() {
		Assert.assertEquals("/8", urlCleaner.clean("/\u2078"));
		Assert.assertEquals("/8", urlCleaner.clean("/\u2088"));
		Assert.assertEquals("/8", urlCleaner.clean("/\u2467"));
		Assert.assertEquals("/8", urlCleaner.clean("/\u24FC"));
		Assert.assertEquals("/8", urlCleaner.clean("/\u277D"));
		Assert.assertEquals("/8", urlCleaner.clean("/\u2787"));
		Assert.assertEquals("/8", urlCleaner.clean("/\u2791"));
		Assert.assertEquals("/8", urlCleaner.clean("/\uFF18"));
		Assert.assertEquals("/8-", urlCleaner.clean("/\u248F"));
	}

	@Test
	public void testSpecialCharsOf9() {
		Assert.assertEquals("/9", urlCleaner.clean("/\u2079"));
		Assert.assertEquals("/9", urlCleaner.clean("/\u2089"));
		Assert.assertEquals("/9", urlCleaner.clean("/\u2468"));
		Assert.assertEquals("/9", urlCleaner.clean("/\u24FD"));
		Assert.assertEquals("/9", urlCleaner.clean("/\u277E"));
		Assert.assertEquals("/9", urlCleaner.clean("/\u2788"));
		Assert.assertEquals("/9", urlCleaner.clean("/\u2792"));
		Assert.assertEquals("/9", urlCleaner.clean("/\uFF19"));
		Assert.assertEquals("/9-", urlCleaner.clean("/\u2490"));
	}

	@Test
	public void testSpecialCharsOf10() {
		Assert.assertEquals("/10", urlCleaner.clean("/\u2469"));
		Assert.assertEquals("/10", urlCleaner.clean("/\u24FE"));
		Assert.assertEquals("/10", urlCleaner.clean("/\u277F"));
		Assert.assertEquals("/10", urlCleaner.clean("/\u2789"));
		Assert.assertEquals("/10", urlCleaner.clean("/\u2793"));
		Assert.assertEquals("/10-", urlCleaner.clean("/\u2491"));
	}

	@Test
	public void testSpecialCharsOf11() {
		Assert.assertEquals("/11", urlCleaner.clean("/\u246A"));
		Assert.assertEquals("/11", urlCleaner.clean("/\u24EB"));
		Assert.assertEquals("/11-", urlCleaner.clean("/\u2492"));
	}

	@Test
	public void testSpecialCharsOf12() {
		Assert.assertEquals("/12", urlCleaner.clean("/\u246B"));
		Assert.assertEquals("/12", urlCleaner.clean("/\u24EC"));
		Assert.assertEquals("/12-", urlCleaner.clean("/\u2493"));
	}

	@Test
	public void testSpecialCharsOf13() {
		Assert.assertEquals("/13", urlCleaner.clean("/\u246C"));
		Assert.assertEquals("/13", urlCleaner.clean("/\u24ED"));
		Assert.assertEquals("/13-", urlCleaner.clean("/\u2494"));
	}

	@Test
	public void testSpecialCharsOf14() {
		Assert.assertEquals("/14", urlCleaner.clean("/\u246D"));
		Assert.assertEquals("/14", urlCleaner.clean("/\u24EE"));
		Assert.assertEquals("/14-", urlCleaner.clean("/\u2495"));
	}

	@Test
	public void testSpecialCharsOf15() {
		Assert.assertEquals("/15", urlCleaner.clean("/\u246E"));
		Assert.assertEquals("/15", urlCleaner.clean("/\u24EF"));
		Assert.assertEquals("/15-", urlCleaner.clean("/\u2496"));
	}

	@Test
	public void testSpecialCharsOf16() {
		Assert.assertEquals("/16", urlCleaner.clean("/\u246F"));
		Assert.assertEquals("/16", urlCleaner.clean("/\u24F0"));
		Assert.assertEquals("/16-", urlCleaner.clean("/\u2497"));
	}

	@Test
	public void testSpecialCharsOf17() {
		Assert.assertEquals("/17", urlCleaner.clean("/\u2470"));
		Assert.assertEquals("/17", urlCleaner.clean("/\u24F1"));
		Assert.assertEquals("/17-", urlCleaner.clean("/\u2498"));
	}

	@Test
	public void testSpecialCharsOf18() {
		Assert.assertEquals("/18", urlCleaner.clean("/\u2471"));
		Assert.assertEquals("/18", urlCleaner.clean("/\u24F2"));
		Assert.assertEquals("/18-", urlCleaner.clean("/\u2499"));
	}

	@Test
	public void testSpecialCharsOf19() {
		Assert.assertEquals("/19", urlCleaner.clean("/\u2472"));
		Assert.assertEquals("/19", urlCleaner.clean("/\u24F3"));
		Assert.assertEquals("/19-", urlCleaner.clean("/\u249A"));
	}
	
	@Test
	public void testSpecialCharsOf20() {
		Assert.assertEquals("/20", urlCleaner.clean("/\u2473"));
		Assert.assertEquals("/20", urlCleaner.clean("/\u24F4"));
		Assert.assertEquals("/20-", urlCleaner.clean("/\u249B"));
	}

	@Test
	public void testSpecialCharsOfA() {
		Assert.assertEquals("/A", urlCleaner.clean("/\u00C0"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u00C1"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u00C2"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u00C3"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u00C5"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u0100"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u0102"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u0104"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u018F"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u01CD"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u01DE"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u01E0"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u01FA"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u0200"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u0202"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u0226"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u023A"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1D00"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1E00"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1EA0"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1EA2"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1EA4"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1EA6"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1EA8"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1EAA"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1EAC"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1EAE"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1EB0"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1EB2"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1EB4"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u1EB6"));
		Assert.assertEquals("/A", urlCleaner.clean("/\u24B6"));
		Assert.assertEquals("/A", urlCleaner.clean("/\uFF21"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u00E0"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u00E1"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u00E2"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u00E3"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u00E5"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u0101"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u0103"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u0105"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u01CE"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u01DF"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u01E1"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u01FB"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u0201"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u0203"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u0227"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u0250"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u0259"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u025A"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1D8F"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1D95"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1E01"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1E9A"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1EA1"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1EA3"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1EA5"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1EA7"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1EA9"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1EAB"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1EAD"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1EAF"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1EB1"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1EB3"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1EB5"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u1EB7"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u2090"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u2094"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u24D0"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u2C65"));
		Assert.assertEquals("/a", urlCleaner.clean("/\u2C6F"));
		Assert.assertEquals("/a", urlCleaner.clean("/\uFF41"));
	}

	@Test
	public void testSpecialCharsOfAA() {
		Assert.assertEquals("/AA", urlCleaner.clean("/\uA732"));
		Assert.assertEquals("/aa", urlCleaner.clean("/\uA733"));
	}

	@Test
	public void testSpecialCharsOfAE() {
		Assert.assertEquals("/Ae", urlCleaner.clean("/\u00C4"));
		Assert.assertEquals("/ae", urlCleaner.clean("/\u00E4"));
		Assert.assertEquals("/ae", urlCleaner.clean("/\u00E6"));
		Assert.assertEquals("/ae", urlCleaner.clean("/\u01E3"));
		Assert.assertEquals("/ae", urlCleaner.clean("/\u01FD"));
		Assert.assertEquals("/ae", urlCleaner.clean("/\u1D02"));
		Assert.assertEquals("/AE", urlCleaner.clean("/\u00C6"));
		Assert.assertEquals("/AE", urlCleaner.clean("/\u01E2"));
		Assert.assertEquals("/AE", urlCleaner.clean("/\u01FC"));
		Assert.assertEquals("/AE", urlCleaner.clean("/\u1D01"));
	}

	@Test
	public void testSpecialCharsOfAO() {
		Assert.assertEquals("/AO", urlCleaner.clean("/\uA734"));
		Assert.assertEquals("/ao", urlCleaner.clean("/\uA735"));
	}

	@Test
	public void testSpecialCharsOfAU() {
		Assert.assertEquals("/AU", urlCleaner.clean("/\uA736"));
		Assert.assertEquals("/au", urlCleaner.clean("/\uA737"));
	}

	@Test
	public void testSpecialCharsOfAV() {
		Assert.assertEquals("/AV", urlCleaner.clean("/\uA738"));
		Assert.assertEquals("/AV", urlCleaner.clean("/\uA73A"));
		Assert.assertEquals("/av", urlCleaner.clean("/\uA739"));
		Assert.assertEquals("/av", urlCleaner.clean("/\uA73B"));
	}

	@Test
	public void testSpecialCharsOfAY() {
		Assert.assertEquals("/AY", urlCleaner.clean("/\uA73C"));
		Assert.assertEquals("/ay", urlCleaner.clean("/\uA73D"));
	}

	@Test
	public void testSpecialCharsOfB() {
		Assert.assertEquals("/B", urlCleaner.clean("/\u0181"));
		Assert.assertEquals("/B", urlCleaner.clean("/\u0182"));
		Assert.assertEquals("/B", urlCleaner.clean("/\u0243"));
		Assert.assertEquals("/B", urlCleaner.clean("/\u0299"));
		Assert.assertEquals("/B", urlCleaner.clean("/\u1D03"));
		Assert.assertEquals("/B", urlCleaner.clean("/\u1E02"));
		Assert.assertEquals("/B", urlCleaner.clean("/\u1E04"));
		Assert.assertEquals("/B", urlCleaner.clean("/\u1E06"));
		Assert.assertEquals("/B", urlCleaner.clean("/\u24B7"));
		Assert.assertEquals("/B", urlCleaner.clean("/\uFF22"));
		Assert.assertEquals("/b", urlCleaner.clean("/\u0180"));
		Assert.assertEquals("/b", urlCleaner.clean("/\u0183"));
		Assert.assertEquals("/b", urlCleaner.clean("/\u0253"));
		Assert.assertEquals("/b", urlCleaner.clean("/\u1D6C"));
		Assert.assertEquals("/b", urlCleaner.clean("/\u1D80"));
		Assert.assertEquals("/b", urlCleaner.clean("/\u1E03"));
		Assert.assertEquals("/b", urlCleaner.clean("/\u1E05"));
		Assert.assertEquals("/b", urlCleaner.clean("/\u1E07"));
		Assert.assertEquals("/b", urlCleaner.clean("/\u24D1"));
		Assert.assertEquals("/b", urlCleaner.clean("/\uFF42"));
	}

	@Test
	public void testSpecialCharsOfC() {
		Assert.assertEquals("/C", urlCleaner.clean("/\u00C7"));
		Assert.assertEquals("/C", urlCleaner.clean("/\u0106"));
		Assert.assertEquals("/C", urlCleaner.clean("/\u0108"));
		Assert.assertEquals("/C", urlCleaner.clean("/\u010A"));
		Assert.assertEquals("/C", urlCleaner.clean("/\u010C"));
		Assert.assertEquals("/C", urlCleaner.clean("/\u0187"));
		Assert.assertEquals("/C", urlCleaner.clean("/\u023B"));
		Assert.assertEquals("/C", urlCleaner.clean("/\u0297"));
		Assert.assertEquals("/C", urlCleaner.clean("/\u1D04"));
		Assert.assertEquals("/C", urlCleaner.clean("/\u1E08"));
		Assert.assertEquals("/C", urlCleaner.clean("/\u24B8"));
		Assert.assertEquals("/C", urlCleaner.clean("/\uFF23"));
		Assert.assertEquals("/c", urlCleaner.clean("/\u00E7"));
		Assert.assertEquals("/c", urlCleaner.clean("/\u0107"));
		Assert.assertEquals("/c", urlCleaner.clean("/\u0109"));
		Assert.assertEquals("/c", urlCleaner.clean("/\u010B"));
		Assert.assertEquals("/c", urlCleaner.clean("/\u010D"));
		Assert.assertEquals("/c", urlCleaner.clean("/\u0188"));
		Assert.assertEquals("/c", urlCleaner.clean("/\u023C"));
		Assert.assertEquals("/c", urlCleaner.clean("/\u0255"));
		Assert.assertEquals("/c", urlCleaner.clean("/\u1E09"));
		Assert.assertEquals("/c", urlCleaner.clean("/\u2184"));
		Assert.assertEquals("/c", urlCleaner.clean("/\u24D2"));
		Assert.assertEquals("/c", urlCleaner.clean("/\uA73E"));
		Assert.assertEquals("/c", urlCleaner.clean("/\uA73F"));
		Assert.assertEquals("/c", urlCleaner.clean("/\uFF43"));
	}

	@Test
	public void testSpecialCharsOfD() {
		Assert.assertEquals("/D", urlCleaner.clean("/\u00D0"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u010E"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u0110"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u0189"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u018A"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u018B"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u1D05"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u1D06"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u1E0A"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u1E0C"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u1E0E"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u1E10"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u1E12"));
		Assert.assertEquals("/D", urlCleaner.clean("/\u24B9"));
		Assert.assertEquals("/D", urlCleaner.clean("/\uA779"));
		Assert.assertEquals("/D", urlCleaner.clean("/\uFF24"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u00F0"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u010F"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u0111"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u018C"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u0221"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u0256"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u0257"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u1D6D"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u1D81"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u1D91"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u1E0B"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u1E0D"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u1E0F"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u1E11"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u1E13"));
		Assert.assertEquals("/d", urlCleaner.clean("/\u24D3"));
		Assert.assertEquals("/d", urlCleaner.clean("/\uA77A"));
		Assert.assertEquals("/d", urlCleaner.clean("/\uFF44"));
	}

	@Test
	public void testSpecialCharsOfDB() {
		Assert.assertEquals("/db", urlCleaner.clean("/\u0238"));
	}

	@Test
	public void testSpecialCharsOfDZ() {
		Assert.assertEquals("/DZ", urlCleaner.clean("/\u01C4"));
		Assert.assertEquals("/DZ", urlCleaner.clean("/\u01F1"));
		Assert.assertEquals("/dz", urlCleaner.clean("/\u01C5"));
		Assert.assertEquals("/dz", urlCleaner.clean("/\u01F2"));
		Assert.assertEquals("/dz", urlCleaner.clean("/\u01C6"));
		Assert.assertEquals("/dz", urlCleaner.clean("/\u01F3"));
		Assert.assertEquals("/dz", urlCleaner.clean("/\u02A3"));
		Assert.assertEquals("/dz", urlCleaner.clean("/\u02A5"));
	}

	@Test
	public void testSpecialCharsOfE() {
		Assert.assertEquals("/E", urlCleaner.clean("/\u00C8"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u00C9"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u00CA"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u00CB"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u0112"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u0114"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u0116"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u0118"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u011A"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u018E"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u0190"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u0204"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u0206"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u0228"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u0246"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1D07"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1E14"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1E16"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1E18"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1E1A"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1E1C"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1EB8"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1EBA"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1EBC"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1EBE"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1EC0"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1EC2"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1EC4"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u1EC6"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u24BA"));
		Assert.assertEquals("/E", urlCleaner.clean("/\u2C7B"));
		Assert.assertEquals("/E", urlCleaner.clean("/\uFF25"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u00E8"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u00E9"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u00EA"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u00EB"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u0113"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u0115"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u0117"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u0119"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u011B"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u01DD"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u0205"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u0207"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u0229"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u0247"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u0258"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u025B"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u025C"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u025D"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u025E"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u029A"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1D08"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1D92"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1D93"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1D94"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1E15"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1E17"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1E19"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1E1B"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1E1D"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1EB9"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1EBB"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1EBD"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1EBF"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1EC1"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1EC3"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1EC5"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u1EC7"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u2091"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u24D4"));
		Assert.assertEquals("/e", urlCleaner.clean("/\u2C78"));
		Assert.assertEquals("/e", urlCleaner.clean("/\uFF45"));
	}

	@Test
	public void testSpecialCharsOfF() {
		Assert.assertEquals("/F", urlCleaner.clean("/\u0191"));
		Assert.assertEquals("/F", urlCleaner.clean("/\u1E1E"));
		Assert.assertEquals("/F", urlCleaner.clean("/\u24BB"));
		Assert.assertEquals("/F", urlCleaner.clean("/\uA730"));
		Assert.assertEquals("/F", urlCleaner.clean("/\uA77B"));
		Assert.assertEquals("/F", urlCleaner.clean("/\uA7FB"));
		Assert.assertEquals("/F", urlCleaner.clean("/\uFF26"));
		Assert.assertEquals("/f", urlCleaner.clean("/\u0192"));
		Assert.assertEquals("/f", urlCleaner.clean("/\u1D6E"));
		Assert.assertEquals("/f", urlCleaner.clean("/\u1D82"));
		Assert.assertEquals("/f", urlCleaner.clean("/\u1E1F"));
		Assert.assertEquals("/f", urlCleaner.clean("/\u1E9B"));
		Assert.assertEquals("/f", urlCleaner.clean("/\u24D5"));
		Assert.assertEquals("/f", urlCleaner.clean("/\uA77C"));
		Assert.assertEquals("/f", urlCleaner.clean("/\uFF46"));
	}

	@Test
	public void testSpecialCharsOfFF() {
		Assert.assertEquals("/ff", urlCleaner.clean("/\uFB00"));
	}

	@Test
	public void testSpecialCharsOfFFI() {
		Assert.assertEquals("/ffi", urlCleaner.clean("/\uFB03"));
	}

	@Test
	public void testSpecialCharsOfFFL() {
		Assert.assertEquals("/ffl", urlCleaner.clean("/\uFB04"));

	}

	@Test
	public void testSpecialCharsOfFI() {
		Assert.assertEquals("/fi", urlCleaner.clean("/\uFB01"));
	}


	@Test
	public void testSpecialCharsOfFL() {
		Assert.assertEquals("/fl", urlCleaner.clean("/\uFB02"));
	}
	

	@Test
	public void testSpecialCharsOfFT() {
		Assert.assertEquals("/ft", urlCleaner.clean("/\uFB05"));
	}

	@Test
	public void testSpecialCharsOfG() {
		Assert.assertEquals("/G", urlCleaner.clean("/\u011C"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u011E"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u0120"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u0122"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u0193"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u01E4"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u01E5"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u01E6"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u01E7"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u01F4"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u0262"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u029B"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u1E20"));
		Assert.assertEquals("/G", urlCleaner.clean("/\u24BC"));
		Assert.assertEquals("/G", urlCleaner.clean("/\uA77D"));
		Assert.assertEquals("/G", urlCleaner.clean("/\uA77E"));
		Assert.assertEquals("/G", urlCleaner.clean("/\uFF27"));
		Assert.assertEquals("/g", urlCleaner.clean("/\u011D"));
		Assert.assertEquals("/g", urlCleaner.clean("/\u011F"));
		Assert.assertEquals("/g", urlCleaner.clean("/\u0121"));
		Assert.assertEquals("/g", urlCleaner.clean("/\u0123"));
		Assert.assertEquals("/g", urlCleaner.clean("/\u01F5"));
		Assert.assertEquals("/g", urlCleaner.clean("/\u0260"));
		Assert.assertEquals("/g", urlCleaner.clean("/\u0261"));
		Assert.assertEquals("/g", urlCleaner.clean("/\u1D77"));
		Assert.assertEquals("/g", urlCleaner.clean("/\u1D79"));
		Assert.assertEquals("/g", urlCleaner.clean("/\u1D83"));
		Assert.assertEquals("/g", urlCleaner.clean("/\u1E21"));
		Assert.assertEquals("/g", urlCleaner.clean("/\u24D6"));
		Assert.assertEquals("/g", urlCleaner.clean("/\uA77F"));
		Assert.assertEquals("/g", urlCleaner.clean("/\uFF47"));
	}

	@Test
	public void testSpecialCharsOfH() {
		Assert.assertEquals("/H", urlCleaner.clean("/\u0124"));
		Assert.assertEquals("/H", urlCleaner.clean("/\u0126"));
		Assert.assertEquals("/H", urlCleaner.clean("/\u021E"));
		Assert.assertEquals("/H", urlCleaner.clean("/\u029C"));
		Assert.assertEquals("/H", urlCleaner.clean("/\u1E22"));
		Assert.assertEquals("/H", urlCleaner.clean("/\u1E24"));
		Assert.assertEquals("/H", urlCleaner.clean("/\u1E26"));
		Assert.assertEquals("/H", urlCleaner.clean("/\u1E28"));
		Assert.assertEquals("/H", urlCleaner.clean("/\u1E2A"));
		Assert.assertEquals("/H", urlCleaner.clean("/\u24BD"));
		Assert.assertEquals("/H", urlCleaner.clean("/\u2C67"));
		Assert.assertEquals("/H", urlCleaner.clean("/\u2C75"));
		Assert.assertEquals("/H", urlCleaner.clean("/\uFF28"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u0125"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u0127"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u021F"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u0265"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u0266"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u02AE"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u02AF"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u1E23"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u1E25"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u1E27"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u1E29"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u1E2B"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u1E96"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u24D7"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u2C68"));
		Assert.assertEquals("/h", urlCleaner.clean("/\u2C76"));
		Assert.assertEquals("/h", urlCleaner.clean("/\uFF48"));
	}

	@Test
	public void testSpecialCharsOfHV() {
		Assert.assertEquals("/HV", urlCleaner.clean("/\u01F6"));
		Assert.assertEquals("/hv", urlCleaner.clean("/\u0195"));
	}

	@Test
	public void testSpecialCharsOfI() {
		Assert.assertEquals("/I", urlCleaner.clean("/\u00CC"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u00CD"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u00CE"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u00CF"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u0128"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u012A"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u012C"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u012E"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u0130"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u0196"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u0197"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u01CF"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u0208"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u020A"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u026A"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u1D7B"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u1E2C"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u1E2E"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u1EC8"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u1ECA"));
		Assert.assertEquals("/I", urlCleaner.clean("/\u24BE"));
		Assert.assertEquals("/I", urlCleaner.clean("/\uA7FE"));
		Assert.assertEquals("/I", urlCleaner.clean("/\uFF29"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u00EC"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u00ED"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u00EE"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u00EF"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u0129"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u012B"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u012D"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u012F"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u0131"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u01D0"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u0209"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u020B"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u0268"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u1D09"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u1D62"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u1D7C"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u1D96"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u1E2D"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u1E2F"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u1EC9"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u1ECB"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u2071"));
		Assert.assertEquals("/i", urlCleaner.clean("/\u24D8"));
		Assert.assertEquals("/i", urlCleaner.clean("/\uFF49"));
	}

	@Test
	public void testSpecialCharsOfIJ() {
		Assert.assertEquals("/IJ", urlCleaner.clean("/\u0132"));
		Assert.assertEquals("/ij", urlCleaner.clean("/\u0133"));
	}

	@Test
	public void testSpecialCharsOfJ() {
		Assert.assertEquals("/J", urlCleaner.clean("/\u0134"));
		Assert.assertEquals("/J", urlCleaner.clean("/\u0248"));
		Assert.assertEquals("/J", urlCleaner.clean("/\u1D0A"));
		Assert.assertEquals("/J", urlCleaner.clean("/\u24BF"));
		Assert.assertEquals("/J", urlCleaner.clean("/\uFF2A"));
		Assert.assertEquals("/j", urlCleaner.clean("/\u0135"));
		Assert.assertEquals("/j", urlCleaner.clean("/\u01F0"));
		Assert.assertEquals("/j", urlCleaner.clean("/\u0237"));
		Assert.assertEquals("/j", urlCleaner.clean("/\u0249"));
		Assert.assertEquals("/j", urlCleaner.clean("/\u025F"));
		Assert.assertEquals("/j", urlCleaner.clean("/\u0284"));
		Assert.assertEquals("/j", urlCleaner.clean("/\u029D"));
		Assert.assertEquals("/j", urlCleaner.clean("/\u24D9"));
		Assert.assertEquals("/j", urlCleaner.clean("/\u2C7C"));
		Assert.assertEquals("/j", urlCleaner.clean("/\uFF4A"));
	}

	@Test
	public void testSpecialCharsOfK() {
		Assert.assertEquals("/K", urlCleaner.clean("/\u0136"));
		Assert.assertEquals("/K", urlCleaner.clean("/\u0198"));
		Assert.assertEquals("/K", urlCleaner.clean("/\u01E8"));
		Assert.assertEquals("/K", urlCleaner.clean("/\u1D0B"));
		Assert.assertEquals("/K", urlCleaner.clean("/\u1E30"));
		Assert.assertEquals("/K", urlCleaner.clean("/\u1E32"));
		Assert.assertEquals("/K", urlCleaner.clean("/\u1E34"));
		Assert.assertEquals("/K", urlCleaner.clean("/\u24C0"));
		Assert.assertEquals("/K", urlCleaner.clean("/\u2C69"));
		Assert.assertEquals("/K", urlCleaner.clean("/\uA740"));
		Assert.assertEquals("/K", urlCleaner.clean("/\uA742"));
		Assert.assertEquals("/K", urlCleaner.clean("/\uA744"));
		Assert.assertEquals("/K", urlCleaner.clean("/\uFF2B"));
		Assert.assertEquals("/k", urlCleaner.clean("/\u0137"));
		Assert.assertEquals("/k", urlCleaner.clean("/\u0199"));
		Assert.assertEquals("/k", urlCleaner.clean("/\u01E9"));
		Assert.assertEquals("/k", urlCleaner.clean("/\u029E"));
		Assert.assertEquals("/k", urlCleaner.clean("/\u1D84"));
		Assert.assertEquals("/k", urlCleaner.clean("/\u1E31"));
		Assert.assertEquals("/k", urlCleaner.clean("/\u1E33"));
		Assert.assertEquals("/k", urlCleaner.clean("/\u1E35"));
		Assert.assertEquals("/k", urlCleaner.clean("/\u24DA"));
		Assert.assertEquals("/k", urlCleaner.clean("/\u2C6A"));
		Assert.assertEquals("/k", urlCleaner.clean("/\uA741"));
		Assert.assertEquals("/k", urlCleaner.clean("/\uA743"));
		Assert.assertEquals("/k", urlCleaner.clean("/\uA745"));
		Assert.assertEquals("/k", urlCleaner.clean("/\uFF4B"));
	}

	@Test
	public void testSpecialCharsOfL() {
		Assert.assertEquals("/L", urlCleaner.clean("/\u0139"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u013B"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u013D"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u013F"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u0141"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u023D"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u029F"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u1D0C"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u1E36"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u1E38"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u1E3A"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u1E3C"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u24C1"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u2C60"));
		Assert.assertEquals("/L", urlCleaner.clean("/\u2C62"));
		Assert.assertEquals("/L", urlCleaner.clean("/\uA746"));
		Assert.assertEquals("/L", urlCleaner.clean("/\uA748"));
		Assert.assertEquals("/L", urlCleaner.clean("/\uA780"));
		Assert.assertEquals("/L", urlCleaner.clean("/\uFF2C"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u013A"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u013C"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u013E"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u0140"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u0142"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u019A"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u0234"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u026B"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u026C"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u026D"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u1D85"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u1E37"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u1E39"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u1E3B"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u1E3D"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u24DB"));
		Assert.assertEquals("/l", urlCleaner.clean("/\u2C61"));
		Assert.assertEquals("/l", urlCleaner.clean("/\uA747"));
		Assert.assertEquals("/l", urlCleaner.clean("/\uA749"));
		Assert.assertEquals("/l", urlCleaner.clean("/\uA781"));
		Assert.assertEquals("/l", urlCleaner.clean("/\uFF4C"));
	}


	@Test
	public void testSpecialCharsOfLJ() {
		Assert.assertEquals("/LJ", urlCleaner.clean("/\u01C7"));
		Assert.assertEquals("/Lj", urlCleaner.clean("/\u01C8"));
		Assert.assertEquals("/lj", urlCleaner.clean("/\u01C9"));
	}

	@Test
	public void testSpecialCharsOfLL() {
		Assert.assertEquals("/LL", urlCleaner.clean("/\u1EFA"));
		Assert.assertEquals("/ll", urlCleaner.clean("/\u1EFB"));
	}

	@Test
	public void testSpecialCharsOfLS() {
		Assert.assertEquals("/ls", urlCleaner.clean("/\u02AA"));
	}

	@Test
	public void testSpecialCharsOfLZ() {
		Assert.assertEquals("/lz", urlCleaner.clean("/\u02AB"));
	}

	@Test
	public void testSpecialCharsOfM() {
		Assert.assertEquals("/M", urlCleaner.clean("/\u019C"));
		Assert.assertEquals("/M", urlCleaner.clean("/\u1D0D"));
		Assert.assertEquals("/M", urlCleaner.clean("/\u1E3E"));
		Assert.assertEquals("/M", urlCleaner.clean("/\u1E40"));
		Assert.assertEquals("/M", urlCleaner.clean("/\u1E42"));
		Assert.assertEquals("/M", urlCleaner.clean("/\u24C2"));
		Assert.assertEquals("/M", urlCleaner.clean("/\u2C6E"));
		Assert.assertEquals("/M", urlCleaner.clean("/\uA7FD"));
		Assert.assertEquals("/M", urlCleaner.clean("/\uA7FF"));
		Assert.assertEquals("/M", urlCleaner.clean("/\uFF2D"));
		Assert.assertEquals("/m", urlCleaner.clean("/\u026F"));
		Assert.assertEquals("/m", urlCleaner.clean("/\u0270"));
		Assert.assertEquals("/m", urlCleaner.clean("/\u0271"));
		Assert.assertEquals("/m", urlCleaner.clean("/\u1D6F"));
		Assert.assertEquals("/m", urlCleaner.clean("/\u1D86"));
		Assert.assertEquals("/m", urlCleaner.clean("/\u1E3F"));
		Assert.assertEquals("/m", urlCleaner.clean("/\u1E41"));
		Assert.assertEquals("/m", urlCleaner.clean("/\u1E43"));
		Assert.assertEquals("/m", urlCleaner.clean("/\u24DC"));
		Assert.assertEquals("/m", urlCleaner.clean("/\uFF4D"));
	}

	@Test
	public void testSpecialCharsOfN() {
		Assert.assertEquals("/N", urlCleaner.clean("/\u00D1"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u0143"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u0145"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u0147"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u014A"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u019D"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u01F8"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u0220"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u0274"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u1D0E"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u1E44"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u1E46"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u1E48"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u1E4A"));
		Assert.assertEquals("/N", urlCleaner.clean("/\u24C3"));
		Assert.assertEquals("/N", urlCleaner.clean("/\uFF2E"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u00F1"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u0144"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u0146"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u0148"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u0149"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u014B"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u019E"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u01F9"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u0235"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u0272"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u0273"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u1D70"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u1D87"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u1E45"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u1E47"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u1E49"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u1E4B"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u207F"));
		Assert.assertEquals("/n", urlCleaner.clean("/\u24DD"));
		Assert.assertEquals("/n", urlCleaner.clean("/\uFF4E"));
	}

	@Test
	public void testSpecialCharsOfNJ() {
		Assert.assertEquals("/NJ", urlCleaner.clean("/\u01CA"));
		Assert.assertEquals("/nj", urlCleaner.clean("/\u01CC"));
	}

	@Test
	public void testSpecialCharsOfO() {
		
		Assert.assertEquals("/O", urlCleaner.clean("/\u00D2"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u00D3"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u00D4"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u00D5"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u00D8"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u014C"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u014E"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u0150"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u0186"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u019F"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u01A0"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u01D1"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u01EA"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u01EC"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u01FE"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u020C"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u020E"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u022A"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u022C"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u022E"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u0230"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1D0F"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1D10"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1E4C"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1E4E"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1E50"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1E52"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1ECC"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1ECE"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1ED0"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1ED2"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1ED4"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1ED6"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1ED8"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1EDA"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1EDC"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1EDE"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1EE0"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u1EE2"));
		Assert.assertEquals("/O", urlCleaner.clean("/\u24C4"));
		Assert.assertEquals("/O", urlCleaner.clean("/\uA74A"));
		Assert.assertEquals("/O", urlCleaner.clean("/\uA74C"));
		Assert.assertEquals("/O", urlCleaner.clean("/\uFF2F"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u00F2"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u00F3"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u00F4"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u00F5"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u00F8"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u014D"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u014F"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u0151"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u01A1"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u01D2"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u01EB"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u01ED"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u01FF"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u020D"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u020F"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u022B"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u022D"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u022F"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u0231"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u0254"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u0275"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1D16"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1D17"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1D97"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1E4D"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1E4F"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1E51"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1E53"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1ECD"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1ECF"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1ED1"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1ED3"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1ED5"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1ED7"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1ED9"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1EDB"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1EDD"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1EDF"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1EE1"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u1EE3"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u2092"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u24DE"));
		Assert.assertEquals("/o", urlCleaner.clean("/\u2C7A"));
		Assert.assertEquals("/o", urlCleaner.clean("/\uA74B"));
		Assert.assertEquals("/o", urlCleaner.clean("/\uA74D"));
		Assert.assertEquals("/o", urlCleaner.clean("/\uFF4F"));
	}

	@Test
	public void testSpecialCharsOfOE() {
		Assert.assertEquals("/OE", urlCleaner.clean("/\u0152"));
		Assert.assertEquals("/OE", urlCleaner.clean("/\u0276"));
		Assert.assertEquals("/Oe", urlCleaner.clean("/\u00D6"));
		Assert.assertEquals("/oe", urlCleaner.clean("/\u00F6"));
		Assert.assertEquals("/oe", urlCleaner.clean("/\u0153"));
		Assert.assertEquals("/oe", urlCleaner.clean("/\u1D14"));
	}

	@Test
	public void testSpecialCharsOfOO() {
		Assert.assertEquals("/OO", urlCleaner.clean("/\uA74E"));
		Assert.assertEquals("/oo", urlCleaner.clean("/\uA74F"));
	}

	@Test
	public void testSpecialCharsOfOU() {
		Assert.assertEquals("/OU", urlCleaner.clean("/\u0222"));
		Assert.assertEquals("/OU", urlCleaner.clean("/\u1D15"));
		Assert.assertEquals("/ou", urlCleaner.clean("/\u0223"));
	}

	@Test
	public void testSpecialCharsOfP() {
		Assert.assertEquals("/P", urlCleaner.clean("/\u01A4"));
		Assert.assertEquals("/P", urlCleaner.clean("/\u1D18"));
		Assert.assertEquals("/P", urlCleaner.clean("/\u1E54"));
		Assert.assertEquals("/P", urlCleaner.clean("/\u1E56"));
		Assert.assertEquals("/P", urlCleaner.clean("/\u24C5"));
		Assert.assertEquals("/P", urlCleaner.clean("/\u2C63"));
		Assert.assertEquals("/P", urlCleaner.clean("/\uA750"));
		Assert.assertEquals("/P", urlCleaner.clean("/\uA752"));
		Assert.assertEquals("/P", urlCleaner.clean("/\uA754"));
		Assert.assertEquals("/P", urlCleaner.clean("/\uFF30"));
		Assert.assertEquals("/p", urlCleaner.clean("/\u01A5"));
		Assert.assertEquals("/p", urlCleaner.clean("/\u1D71"));
		Assert.assertEquals("/p", urlCleaner.clean("/\u1D7D"));
		Assert.assertEquals("/p", urlCleaner.clean("/\u1D88"));
		Assert.assertEquals("/p", urlCleaner.clean("/\u1E55"));
		Assert.assertEquals("/p", urlCleaner.clean("/\u1E57"));
		Assert.assertEquals("/p", urlCleaner.clean("/\u24DF"));
		Assert.assertEquals("/p", urlCleaner.clean("/\uA751"));
		Assert.assertEquals("/p", urlCleaner.clean("/\uA753"));
		Assert.assertEquals("/p", urlCleaner.clean("/\uA755"));
		Assert.assertEquals("/p", urlCleaner.clean("/\uA7FC"));
		Assert.assertEquals("/p", urlCleaner.clean("/\uFF50"));
	}

	@Test
	public void testSpecialCharsOfQ() {
		Assert.assertEquals("/Q", urlCleaner.clean("/\u024A"));
		Assert.assertEquals("/Q", urlCleaner.clean("/\u24C6"));
		Assert.assertEquals("/Q", urlCleaner.clean("/\uA756"));
		Assert.assertEquals("/Q", urlCleaner.clean("/\uA758"));
		Assert.assertEquals("/Q", urlCleaner.clean("/\uFF31"));
		Assert.assertEquals("/q", urlCleaner.clean("/\u0138"));
		Assert.assertEquals("/q", urlCleaner.clean("/\u024B"));
		Assert.assertEquals("/q", urlCleaner.clean("/\u02A0"));
		Assert.assertEquals("/q", urlCleaner.clean("/\u24E0"));
		Assert.assertEquals("/q", urlCleaner.clean("/\uA757"));
		Assert.assertEquals("/q", urlCleaner.clean("/\uA759"));
		Assert.assertEquals("/q", urlCleaner.clean("/\uFF51"));
	}

	@Test
	public void testSpecialCharsOfQP() {
		Assert.assertEquals("/qp", urlCleaner.clean("/\u0239"));
	}

	@Test
	public void testSpecialCharsOfR() {
		Assert.assertEquals("/R", urlCleaner.clean("/\u0154"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u0156"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u0158"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u0210"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u0212"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u024C"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u0280"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u0281"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u1D19"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u1D1A"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u1E58"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u1E5A"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u1E5C"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u1E5E"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u24C7"));
		Assert.assertEquals("/R", urlCleaner.clean("/\u2C64"));
		Assert.assertEquals("/R", urlCleaner.clean("/\uA75A"));
		Assert.assertEquals("/R", urlCleaner.clean("/\uA782"));
		Assert.assertEquals("/R", urlCleaner.clean("/\uFF32"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u0155"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u0157"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u0159"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u0211"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u0213"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u024D"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u027C"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u027D"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u027E"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u027F"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u1D63"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u1D72"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u1D73"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u1D89"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u1E59"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u1E5B"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u1E5D"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u1E5F"));
		Assert.assertEquals("/r", urlCleaner.clean("/\u24E1"));
		Assert.assertEquals("/r", urlCleaner.clean("/\uA75B"));
		Assert.assertEquals("/r", urlCleaner.clean("/\uA783"));
		Assert.assertEquals("/r", urlCleaner.clean("/\uFF52"));
	}

	@Test
	public void testSpecialCharsOfS() {
		Assert.assertEquals("/S", urlCleaner.clean("/\u015A"));
		Assert.assertEquals("/S", urlCleaner.clean("/\u015C"));
		Assert.assertEquals("/S", urlCleaner.clean("/\u015E"));
		Assert.assertEquals("/S", urlCleaner.clean("/\u0160"));
		Assert.assertEquals("/S", urlCleaner.clean("/\u0218"));
		Assert.assertEquals("/S", urlCleaner.clean("/\u1E60"));
		Assert.assertEquals("/S", urlCleaner.clean("/\u1E62"));
		Assert.assertEquals("/S", urlCleaner.clean("/\u1E64"));
		Assert.assertEquals("/S", urlCleaner.clean("/\u1E66"));
		Assert.assertEquals("/S", urlCleaner.clean("/\u1E68"));
		Assert.assertEquals("/S", urlCleaner.clean("/\u24C8"));
		Assert.assertEquals("/S", urlCleaner.clean("/\uA731"));
		Assert.assertEquals("/S", urlCleaner.clean("/\uA785"));
		Assert.assertEquals("/S", urlCleaner.clean("/\uFF33"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u015B"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u015D"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u015F"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u0161"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u017F"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u0219"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u023F"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u0282"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u1D74"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u1D8A"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u1E61"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u1E63"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u1E65"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u1E67"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u1E69"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u1E9C"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u1E9D"));
		Assert.assertEquals("/s", urlCleaner.clean("/\u24E2"));
		Assert.assertEquals("/s", urlCleaner.clean("/\uA784"));
		Assert.assertEquals("/s", urlCleaner.clean("/\uFF53"));
	}

	@Test
	public void testSpecialCharsOfSS() {
		Assert.assertEquals("/SS", urlCleaner.clean("/\u1E9E"));
		Assert.assertEquals("/ss", urlCleaner.clean("/\u00DF"));
	}

	@Test
	public void testSpecialCharsOfST() {
		Assert.assertEquals("/st", urlCleaner.clean("/\uFB06"));
	}

	@Test
	public void testSpecialCharsOfT() {
		Assert.assertEquals("/T", urlCleaner.clean("/\u0162"));
		Assert.assertEquals("/T", urlCleaner.clean("/\u0164"));
		Assert.assertEquals("/T", urlCleaner.clean("/\u0166"));
		Assert.assertEquals("/T", urlCleaner.clean("/\u01AC"));
		Assert.assertEquals("/T", urlCleaner.clean("/\u01AE"));
		Assert.assertEquals("/T", urlCleaner.clean("/\u021A"));
		Assert.assertEquals("/T", urlCleaner.clean("/\u023E"));
		Assert.assertEquals("/T", urlCleaner.clean("/\u1D1B"));
		Assert.assertEquals("/T", urlCleaner.clean("/\u1E6A"));
		Assert.assertEquals("/T", urlCleaner.clean("/\u1E6C"));
		Assert.assertEquals("/T", urlCleaner.clean("/\u1E6E"));
		Assert.assertEquals("/T", urlCleaner.clean("/\u1E70"));
		Assert.assertEquals("/T", urlCleaner.clean("/\u24C9"));
		Assert.assertEquals("/T", urlCleaner.clean("/\uA786"));
		Assert.assertEquals("/T", urlCleaner.clean("/\uFF34"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u0163"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u0165"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u0167"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u01AB"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u01AD"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u021B"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u0236"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u0287"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u0288"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u1D75"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u1E6B"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u1E6D"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u1E6F"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u1E71"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u1E97"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u24E3"));
		Assert.assertEquals("/t", urlCleaner.clean("/\u2C66"));
		Assert.assertEquals("/t", urlCleaner.clean("/\uFF54"));
	}

	@Test
	public void testSpecialCharsOfTH() {
		Assert.assertEquals("/TH", urlCleaner.clean("/\u00DE"));
		Assert.assertEquals("/TH", urlCleaner.clean("/\uA766"));
		Assert.assertEquals("/th", urlCleaner.clean("/\u00FE"));
		Assert.assertEquals("/th", urlCleaner.clean("/\u1D7A"));
		Assert.assertEquals("/th", urlCleaner.clean("/\uA767"));
	}

	@Test
	public void testSpecialCharsOfTS() {
		Assert.assertEquals("/ts", urlCleaner.clean("/\u02A6"));
	}

	@Test
	public void testSpecialCharsOfTC() {
		Assert.assertEquals("/tc", urlCleaner.clean("/\u02A8"));
	}

	@Test
	public void testSpecialCharsOfTZ() {
		Assert.assertEquals("/TZ", urlCleaner.clean("/\uA728"));
		Assert.assertEquals("/tz", urlCleaner.clean("/\uA729"));
	}

	@Test
	public void testSpecialCharsOfU() {
		Assert.assertEquals("/U", urlCleaner.clean("/\u00D9"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u00DA"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u00DB"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u0168"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u016A"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u016C"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u016E"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u0170"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u0172"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u01AF"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u01D3"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u01D5"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u01D7"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u01D9"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u01DB"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u0214"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u0216"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u0244"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1D1C"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1D7E"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1E72"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1E74"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1E76"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1E78"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1E7A"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1EE4"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1EE6"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1EE8"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1EEA"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1EEC"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1EEE"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u1EF0"));
		Assert.assertEquals("/U", urlCleaner.clean("/\u24CA"));
		Assert.assertEquals("/U", urlCleaner.clean("/\uFF35"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u00F9"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u00FA"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u00FB"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u0169"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u016B"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u016D"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u016F"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u0171"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u0173"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u01B0"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u01D4"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u01D6"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u01D8"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u01DA"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u01DC"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u0215"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u0217"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u0289"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1D64"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1D99"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1E73"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1E75"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1E77"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1E79"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1E7B"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1EE5"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1EE7"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1EE9"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1EEB"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1EED"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1EEF"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u1EF1"));
		Assert.assertEquals("/u", urlCleaner.clean("/\u24E4"));
		Assert.assertEquals("/u", urlCleaner.clean("/\uFF55"));
	}

	@Test
	public void testSpecialCharsOfUE() {
		Assert.assertEquals("/Ue", urlCleaner.clean("/\u00DC"));
		Assert.assertEquals("/ue", urlCleaner.clean("/\u1D6B"));
		Assert.assertEquals("/ue", urlCleaner.clean("/\u00FC"));
	}

	@Test
	public void testSpecialCharsOfV() {
		Assert.assertEquals("/V", urlCleaner.clean("/\u01B2"));
		Assert.assertEquals("/V", urlCleaner.clean("/\u0245"));
		Assert.assertEquals("/V", urlCleaner.clean("/\u1D20"));
		Assert.assertEquals("/V", urlCleaner.clean("/\u1E7C"));
		Assert.assertEquals("/V", urlCleaner.clean("/\u1E7E"));
		Assert.assertEquals("/V", urlCleaner.clean("/\u1EFC"));
		Assert.assertEquals("/V", urlCleaner.clean("/\u24CB"));
		Assert.assertEquals("/V", urlCleaner.clean("/\uA75E"));
		Assert.assertEquals("/V", urlCleaner.clean("/\uA768"));
		Assert.assertEquals("/V", urlCleaner.clean("/\uFF36"));
		Assert.assertEquals("/v", urlCleaner.clean("/\u028B"));
		Assert.assertEquals("/v", urlCleaner.clean("/\u028C"));
		Assert.assertEquals("/v", urlCleaner.clean("/\u1D65"));
		Assert.assertEquals("/v", urlCleaner.clean("/\u1D8C"));
		Assert.assertEquals("/v", urlCleaner.clean("/\u1E7D"));
		Assert.assertEquals("/v", urlCleaner.clean("/\u1E7F"));
		Assert.assertEquals("/v", urlCleaner.clean("/\u24E5"));
		Assert.assertEquals("/v", urlCleaner.clean("/\u2C71"));
		Assert.assertEquals("/v", urlCleaner.clean("/\u2C74"));
		Assert.assertEquals("/v", urlCleaner.clean("/\uA75F"));
		Assert.assertEquals("/v", urlCleaner.clean("/\uFF56"));
	}

	@Test
	public void testSpecialCharsOfVY() {
		Assert.assertEquals("/VY", urlCleaner.clean("/\uA760"));
		Assert.assertEquals("/vy", urlCleaner.clean("/\uA761"));
	}

	@Test
	public void testSpecialCharsOfW() {
		Assert.assertEquals("/W", urlCleaner.clean("/\u0174"));
		Assert.assertEquals("/W", urlCleaner.clean("/\u01F7"));
		Assert.assertEquals("/W", urlCleaner.clean("/\u1D21"));
		Assert.assertEquals("/W", urlCleaner.clean("/\u1E80"));
		Assert.assertEquals("/W", urlCleaner.clean("/\u1E82"));
		Assert.assertEquals("/W", urlCleaner.clean("/\u1E84"));
		Assert.assertEquals("/W", urlCleaner.clean("/\u1E86"));
		Assert.assertEquals("/W", urlCleaner.clean("/\u1E88"));
		Assert.assertEquals("/W", urlCleaner.clean("/\u24CC"));
		Assert.assertEquals("/W", urlCleaner.clean("/\u2C72"));
		Assert.assertEquals("/W", urlCleaner.clean("/\uFF37"));
		Assert.assertEquals("/w", urlCleaner.clean("/\u0175"));
		Assert.assertEquals("/w", urlCleaner.clean("/\u01BF"));
		Assert.assertEquals("/w", urlCleaner.clean("/\u028D"));
		Assert.assertEquals("/w", urlCleaner.clean("/\u1E81"));
		Assert.assertEquals("/w", urlCleaner.clean("/\u1E83"));
		Assert.assertEquals("/w", urlCleaner.clean("/\u1E85"));
		Assert.assertEquals("/w", urlCleaner.clean("/\u1E87"));
		Assert.assertEquals("/w", urlCleaner.clean("/\u1E89"));
		Assert.assertEquals("/w", urlCleaner.clean("/\u1E98"));
		Assert.assertEquals("/w", urlCleaner.clean("/\u24E6"));
		Assert.assertEquals("/w", urlCleaner.clean("/\u2C73"));
		Assert.assertEquals("/w", urlCleaner.clean("/\uFF57"));
	}

	@Test
	public void testSpecialCharsOfX() {
		Assert.assertEquals("/X", urlCleaner.clean("/\u1E8A"));
		Assert.assertEquals("/X", urlCleaner.clean("/\u1E8C"));
		Assert.assertEquals("/X", urlCleaner.clean("/\u24CD"));
		Assert.assertEquals("/X", urlCleaner.clean("/\uFF38"));
		Assert.assertEquals("/x", urlCleaner.clean("/\u1D8D"));
		Assert.assertEquals("/x", urlCleaner.clean("/\u1E8B"));
		Assert.assertEquals("/x", urlCleaner.clean("/\u1E8D"));
		Assert.assertEquals("/x", urlCleaner.clean("/\u2093"));
		Assert.assertEquals("/x", urlCleaner.clean("/\u24E7"));
		Assert.assertEquals("/x", urlCleaner.clean("/\uFF58"));
	}

	@Test
	public void testSpecialCharsOfY() {
		Assert.assertEquals("/Y", urlCleaner.clean("/\u00DD"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u0176"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u0178"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u01B3"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u0232"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u024E"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u028F"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u1E8E"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u1EF2"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u1EF4"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u1EF6"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u1EF8"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u1EFE"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\u24CE"));
		Assert.assertEquals("/Y", urlCleaner.clean("/\uFF39"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u00FD"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u00FF"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u0177"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u01B4"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u0233"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u024F"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u028E"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u1E8F"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u1E99"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u1EF3"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u1EF5"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u1EF7"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u1EF9"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u1EFF"));
		Assert.assertEquals("/y", urlCleaner.clean("/\u24E8"));
		Assert.assertEquals("/y", urlCleaner.clean("/\uFF59"));
	}

	@Test
	public void testSpecialCharsOfZ() {
		Assert.assertEquals("/Z", urlCleaner.clean("/\u0179"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\u017B"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\u017D"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\u01B5"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\u021C"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\u0224"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\u1D22"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\u1E90"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\u1E92"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\u1E94"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\u24CF"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\u2C6B"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\uA762"));
		Assert.assertEquals("/Z", urlCleaner.clean("/\uFF3A"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u017A"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u017C"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u017E"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u01B6"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u021D"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u0225"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u0240"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u0290"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u0291"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u1D76"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u1D8E"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u1E91"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u1E93"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u1E95"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u24E9"));
		Assert.assertEquals("/z", urlCleaner.clean("/\u2C6C"));
		Assert.assertEquals("/z", urlCleaner.clean("/\uA763"));
		Assert.assertEquals("/z", urlCleaner.clean("/\uFF5A"));
	}

	@Test
	public void testUrlSafeSpecialCharacters() {
		Assert.assertEquals("/-.html", urlCleaner.clean("/-.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/_.html"));
		Assert.assertEquals("/-.html", urlCleaner.clean("/~.html"));
	}
}
