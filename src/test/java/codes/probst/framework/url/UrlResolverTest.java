package codes.probst.framework.url;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UrlResolverTest {
	private UrlResolver<Dummy> urlResolver;
	
	@Before
	public void test() {
		AbstractUrlResolver<Dummy> urlResolver = new AbstractUrlResolver<Dummy>() {

			@Override
			protected Map<String, String> getPathParameters(Dummy value) {
				Map<String, String> params = new HashMap<>();
				
				params.put("name", value.getName());
				params.put("value", value.getValue());
				
				return params;
			}
			
			@Override
			protected String getPattern() {
				return "/{name}/{value}.html";
			}
		};

		
		urlResolver.setUrlCleaner(new DefaultUrlCleaner());
		
		this.urlResolver = urlResolver;
	}
	
	@Test
	public void testSpecialCharValue() {
		Assert.assertEquals("/Wurst---Brot/1.html", urlResolver.resolve(new Dummy("Wurst & Brot", "1")));
	}
	
	@Test
	public void testSimpleValue() {
		Assert.assertEquals("/Simple/1234.html", urlResolver.resolve(new Dummy("Simple", "1234")));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNullValue() {
		Assert.assertEquals("//.html", urlResolver.resolve(new Dummy(null, null)));
	}
	
	
	public static class Dummy {
		private String name;
		private String value;
		
		public Dummy(String name, String value) {
			this.name = name;
			this.value = value;
		}
		
		public String getName() {
			return name;
		}
		
		public String getValue() {
			return value;
		}
	}
	
}
