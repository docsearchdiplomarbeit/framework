package codes.probst.framework.populator;

/**
 * Option superclass for {@code Facade}s to define which data must be populated. 
 * @author Benjamin Probst
 *
 */
public abstract class PopulationOption implements Comparable<PopulationOption> {
	private String code;
	private int order;
	
	protected PopulationOption(String code, int order) {
		this.code = code;
		this.order = order;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * Returns the order of a option
	 * @return the order
	 */
	public int getOrder() {
		return order;
	}
	
	/**
	 * Compares two PopulationOptions and use the order and the code to get the compare value.
	 * @return the compare value
	 */
	@Override
	public int compareTo(PopulationOption other) {
		int compare = Integer.compare(getOrder(), other.getOrder());
		if(compare == 0) {
			compare = getCode().compareTo(other.getCode());
		}
		
		return compare;
	}

	/**
	 * Returns the hashCode of this Option based in the code and the order.
	 * @return The hash code
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + order;
		return result;
	}

	/**
	 * Compares this option with the given object and returns {@code true} if the object is not {@code null}
	 * and the code and the order of the given {@code PopulationOption} is equals to this object.
	 * 
	 * @return {@code true} if the given object is equals to this and {@code false} if it is not
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PopulationOption)) {
			return false;
		}
		PopulationOption other = (PopulationOption) obj;
		if (code == null) {
			if (other.getCode() != null) {
				return false;
			}
		} else if (!code.equals(other.getCode())) {
			return false;
		}
		if (order != other.getOrder()) {
			return false;
		}
		return true;
	}

}
