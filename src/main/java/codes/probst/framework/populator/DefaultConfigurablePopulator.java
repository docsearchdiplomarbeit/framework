package codes.probst.framework.populator;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

/**
 * Default implementation of a {@code ConfigurablePopulator} that uses a {@code Map} to handle the {@code PopulationOption}s that are requested.
 * This implementation respects the value of {@code PopulationOption#getOrder()} to call the {@code Populator}s in order.
 * 
 * @author Benjamin Probst
 *
 * @param <S>
 * @param <T>
 */
public class DefaultConfigurablePopulator<S, T> implements ConfigurablePopulator<S, T> {
	private Map<PopulationOption, Collection<Populator<S, T>>> populators;
	
	
	@Override
	public void populate(S source, T target, PopulationOption... options) {
		if(source == null) {
			throw new IllegalArgumentException("source was null");
		}
		if(target == null) {
			throw new IllegalArgumentException("target was null");
		}
		
		Arrays.stream(options).sorted(PopulationOption::compareTo).forEach(option -> {
			Collection<Populator<S, T>> optionPopulators = populators.get(option);
			if(optionPopulators != null && !optionPopulators.isEmpty()) {
				optionPopulators.forEach(populator -> populator.populate(source, target));
			}
		});
	}

	/**
	 * Sets the configured {@code Map} of populators.
	 * @param populators The populator map
	 */
	public void setPopulators(Map<PopulationOption, Collection<Populator<S, T>>> populators) {
		this.populators = populators;
	}

}
