package codes.probst.framework.populator;

/**
 * Populators are used to map objects from the source object to the target object.
 * @author Benjamin Probst
 *
 * @param <S> Source object type
 * @param <T> Target object type
 */
public interface Populator<S, T> {

	/**
	 * Populates the values from the source object to the target object.
	 * @param source Source Object
	 * @param target Target object
	 */
	public void populate(S source, T target);
	
}
