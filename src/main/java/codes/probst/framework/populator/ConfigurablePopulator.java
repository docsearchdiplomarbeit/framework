package codes.probst.framework.populator;

/**
 * Populator that accepts specific options to be populated as requested.
 * @author Benjamin Probst
 *
 * @param <S> Type of the source object
 * @param <T> Type of the target object
 */
public interface ConfigurablePopulator<S, T> {

	/**
	 * Populates values based on the source object to the target object as requested by the given {@code PopulationOption}s.
	 * @param source Source object
	 * @param target Target object
	 * @param options Options to be populated	
	 */
	public void populate(S source, T target, PopulationOption... options);
	
}
