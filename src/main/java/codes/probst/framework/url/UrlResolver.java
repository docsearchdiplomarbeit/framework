package codes.probst.framework.url;

/**
 * Resolves a url based on a object.
 * @author Benjamin Probst
 *
 * @param <T> The object to be resolved as a url
 */
public interface UrlResolver<T> {

	/**
	 * Resolves a url based on the value
	 * @param value The object
	 * @return The url
	 */
	public abstract String resolve(T value);

}