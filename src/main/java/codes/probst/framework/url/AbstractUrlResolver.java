package codes.probst.framework.url;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Resolves a url based on a object by replacing each variable by its value in the parameters map.
 * @author Benjamin Probst
 *
 * @param <T> The type of the object
 */
public abstract class AbstractUrlResolver<T> implements UrlResolver<T> {
	private static final Pattern VARIABLE_PATTERN = Pattern.compile("\\{([^\\})]*)\\}");
	
	private UrlCleaner urlCleaner;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String resolve(T value) {
		String url = buildUrl(getPathParameters(value));
		return urlCleaner.clean(url);
	}
	
	/**
	 * Builds the url by replacing each variable in the pattern with its replacement from the parameter map.
	 * @param pathParameters The parameters to be used
	 * @return The builded url
	 */
	protected String buildUrl(Map<String, String> pathParameters) {
		String url = getPattern();
		
		Matcher matcher = VARIABLE_PATTERN.matcher(url);
		StringBuffer urlBuilder = new StringBuffer(url.length());
		while(matcher.find()) {
			String replacement = pathParameters.get(matcher.group(1));
			if(replacement == null) {
				throw new IllegalArgumentException("variable " + matcher.group(1) + " not satisfied");
			}
			matcher.appendReplacement(urlBuilder, replacement.replace("$", "\\$"));
		}
		matcher.appendTail(urlBuilder);
		
		return urlBuilder.toString().replace("//", "/");
	}
	
	/**
	 * Returns the pattern with variables marked between { and }.
	 * @return The pattern
	 */
	protected abstract String getPattern();
	
	/**
	 * Escapes the value to handle null values.
	 * @param value The object
	 * @return The string representation
	 */
	protected String escapeValue(Object value) {
		return value == null ? "" : value.toString();
	}
	
	/**
	 * Returns the parameters supported by this {@code UrlResolver}.
	 * @param value The Object
	 * @return The map with parameters
	 */
	protected abstract Map<String, String> getPathParameters(T value);
	
	/**
	 * Sets the {@code UrlCleaner}.
	 * @param urlCleaner The cleaner.
	 */
	public void setUrlCleaner(UrlCleaner urlCleaner) {
		this.urlCleaner = urlCleaner;
	}
	
}
