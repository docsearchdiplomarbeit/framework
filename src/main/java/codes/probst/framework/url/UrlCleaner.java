package codes.probst.framework.url;

/**
 * Cleans a given url and make it url safe.
 * @author Benjamin Probst
 *
 */
public interface UrlCleaner {

	/**
	 * Cleans the given url to be url safe.
	 * @param value A url
	 * @return A cleaned url
	 */
	public abstract String clean(String value);

}