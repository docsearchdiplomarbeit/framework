package codes.probst.framework.converter;

/**
 * Converts a Object to an other Object.
 * 
 * @author Benjamin Probst
 *
 * @param <T> Source Object Type
 * @param <R> Target Object Type
 */
public interface Converter<T, R> {

	public R convert(T value);
}
