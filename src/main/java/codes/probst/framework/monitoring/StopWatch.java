package codes.probst.framework.monitoring;

import java.util.concurrent.TimeUnit;

/**
 * Measurement class to measure time between the start and the current or the stop time.
 * 
 * @author Benjamin Probst
 * @since 1.0.0
 *
 */
public class StopWatch {
	private long startTime = -1;
	private long endTime = -1;
	
	/**
	 * Starts the timer.
	 */
	public void start() {
		if (startTime != -1) {
			throw new IllegalStateException("StopWatch already started");
		}
		startTime = getCurrentTime();
	}
	
	/**
	 * Calculates the elapsed time between the call of {@link #start() start} and either {@link #stop() stop} or the current time and returns it in milliseconds.
	 * @return elapsed time in milliseconds
	 */
	public long getElapsedTime() {
		return getElapsedTime(TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Calculates the elapsed time between the call of {@link #start() start} and either {@link #stop() stop} or the current time and returns it in the provided {@code TimeUnit}.
	 * @param unit Unit of the elapsed time
	 * @return elapsed time in the provided unit
	 */
	public long getElapsedTime(TimeUnit unit) {
		if(startTime == -1) {
			throw new IllegalStateException("StopWatch not started");
		}
		
		long endTime = this.endTime == -1 ? getCurrentTime() : this.endTime;
		
		return calculateDifference(startTime, endTime, unit);
	}

	/**
	 * Stops the timer and returns the elapsed time in the provided {@code TimeUnit}.
	 * @param unit Unit of the elapsed time
	 * @return elapsed time in the provided unit
	 */
	public long stop(TimeUnit unit) {
		if(endTime != -1) {
			throw new IllegalStateException("StopWatch already stopped");
		}
		
		endTime = getCurrentTime();
		return getElapsedTime(unit);
	}

	/**
	 * Stops the timer and returns the elapsed time in milliseconds.
	 * @return elapsed time in milliseconds
	 */
	public long stop() {
		return stop(TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Returns a string representation of this {@code StopWatch} with the given {@code TimeUnit}.
	 * @param unit time unit for the elapsed time calculation
	 * @return String representation of this object
	 */
	public String toString(TimeUnit unit) {
		return "StopWatch [startTime=" + startTime + ", endTime=" + endTime + ", elapsedTime=" + getElapsedTime(unit) + "]";
	}


	/**
	 * Returns a string representation of this {@code StopWatch} with elapsed time in milliseconds.
	 * @return a string representation of this {@code StopWatch}
	 */
	@Override
	public String toString() {
		return toString(TimeUnit.MILLISECONDS);
	}

    /**
     * Returns the hash code value for this {@code StopWatch}.
     * @return the hash code value for this {@code StopWatch}
     */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (endTime ^ (endTime >>> 32));
		result = prime * result + (int) (startTime ^ (startTime >>> 32));
		return result;
	}

    /**
     * Compares the specified object with this {@code StopWatch} for equality.  Returns
     * {@code true} if and only if the specified object is also a {@code StopWatch}, both
     * objects have the same start and stop time.
     * @param o the object to be compared for equality with this {@code StopWatch}
     * @return {@code true} if the specified object is equal to this {@code StopWatch}
     */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof StopWatch)) {
			return false;
		}
		StopWatch other = (StopWatch) obj;
		if (endTime != other.endTime) {
			return false;
		}
		if (startTime != other.startTime) {
			return false;
		}
		return true;
	}

	/**
	 * Calculates the difference between two values and converts it form nanoseconds to the given {@code TimeUnit}.
	 * @param start start time
	 * @param end end time
	 * @param unit desired {@code TimeUnit} for the result
	 * @return calculated difference in the given {@code TimeUnit}
	 */
	protected long calculateDifference(long start, long end, TimeUnit unit) {
		return unit.convert(end - start, TimeUnit.NANOSECONDS);
	}
	
	/**
	 * Returns the current time.
	 * @return current time
	 */
	protected long getCurrentTime() {
		return System.nanoTime();
	}
}
