package codes.probst.framework.file.service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.util.zip.ZipInputStream;

/**
 * Service to read data from {@code URL} or get {@code byte}s from a {@code InputStream}.
 * @author Benjamin Probst
 *
 */
public interface FileService {

	/**
	 * Reads data from a {@code URL} and returns all {@code byte}s retrieved.
	 * @param url The url to load
	 * @return The retrieved bytes
	 * @throws IOException If an I/O exception occurs
	 */
	public abstract byte[] getBytesFromUrl(URL url) throws IOException;
	
	/**
	 * Reads all bytes from a {@code InputStream} and returns them.
	 * @param is the InputStream
	 * @return all read bytes
	 * @throws IOException If an I/O exception occurs
	 */
	public abstract byte[] getBytesFromInputStream(InputStream is) throws IOException;

	
	public abstract void extractZipToFolder(ZipInputStream zis, Path path) throws IOException;
}