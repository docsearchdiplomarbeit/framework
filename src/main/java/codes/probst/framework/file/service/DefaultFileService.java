package codes.probst.framework.file.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Implementation of the {@code FileService} with a fixed configurable buffer size.
 * @author Benjamin Probst
 *
 */
public class DefaultFileService implements FileService {
	private int bufferSize;
	
	/**
	 * Constructs a {@code DefaultFileService} with a buffer size of 1024.
	 */
	public DefaultFileService() {
		this(1024);
	}
	
	/**
	 * Constructs a {@code DefaultFileService} with the given buffer size.
	 * @param bufferSize Size of the buffer
	 */
	public DefaultFileService(int bufferSize) {
		this.bufferSize = bufferSize;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getBytesFromUrl(URL url) throws IOException  {
		URLConnection connection = url.openConnection();
		connection.connect();
		
		byte[] bytes = null;
		try(InputStream is = connection.getInputStream()) {
			bytes = getBytesFromInputStream(is);
		}
		
		return bytes;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] getBytesFromInputStream(InputStream is) throws IOException  {
		int bytesRead = 0;
		byte[] buffer = new byte[bufferSize];
		try(ByteArrayOutputStream os = new ByteArrayOutputStream()) {
			while((bytesRead = is.read(buffer)) != -1 ) {
				os.write(buffer, 0, bytesRead);
			}
			
			return os.toByteArray();
		}
	}
	
	@Override
	public void extractZipToFolder(ZipInputStream zis, Path location) throws IOException {
		Files.createDirectories(location);
		ZipEntry entry = null;
		while(null != (entry = zis.getNextEntry())) {
			if(entry.isDirectory()) {
				Files.createDirectories(location.resolve(entry.getName()));
			} else {
				Path currentLocation = location.resolve(entry.getName());
				Files.createDirectories(currentLocation.getParent());
				Files.write(currentLocation, getBytesFromInputStream(zis));
			}
		}
	}
	
}
