package codes.probst.framework.bean;

public interface BeanResolver {

	public <T> T resolve(Class<T> clazz);
	
}
