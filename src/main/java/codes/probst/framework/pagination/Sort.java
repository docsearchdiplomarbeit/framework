package codes.probst.framework.pagination;


/**
 * Represents a sort criteria based on the name of the sort field and its {@code SortDirection}.
 * 
 * @author Benjamin Probst
 *
 */
public class Sort {
	private String name;
	private SortDirection direction;
	
	public Sort() {
	}
	
	public Sort(String name, SortDirection direction) {
		this.name = name;
		this.direction = direction;
	}
	
	/**
	 * Returns the field name.
	 * @return field name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the field name
	 * @param name field name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Returns the sort direction.
	 * @return Sort direction
	 */
	public SortDirection getDirection() {
		return direction;
	}
	
	/**
	 * Sets the sort direction.
	 * @param direction sort direction
	 */
	public void setDirection(SortDirection direction) {
		this.direction = direction;
	}
	
}
