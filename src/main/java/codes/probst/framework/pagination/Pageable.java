package codes.probst.framework.pagination;

/**
 * Represents the information needed to sort and request specific pages of larger result set.
 * 
 * @author Benjamin Probst
 *
 */
public class Pageable {
	protected int pageSize;
	protected Sort[] sort;
	protected int currentPage;

	/**
	 * Returns the size of a page.
	 * @return size of a page
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * Sets the size of a page
	 * @param pageSize size of a page
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * Returns the sort criterias.
	 * @return Sort criterias
	 */
	public Sort[] getSort() {
		return sort;
	}

	/**
	 * Sets the sort criterias.
	 * @param sort Sort criterias
	 */
	public void setSort(Sort[] sort) {
		this.sort = sort;
	}

	/**
	 * Returns the current page.
	 * @return current page
	 */
	public int getCurrentPage() {
		return currentPage;
	}

	/**
	 * Sets the current page.
	 * @param currentPage current page
	 */
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

}
