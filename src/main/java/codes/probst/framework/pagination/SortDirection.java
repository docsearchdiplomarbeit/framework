package codes.probst.framework.pagination;

/**
 * Represents the sort direction of a {@code Sort} object.
 * 
 * @author Benjamin Probst
 *
 */
public enum SortDirection {
	ASCENDING,
	DESCENDING;
}
