package codes.probst.framework.dao;

import java.util.Collection;
import java.util.Optional;

import codes.probst.framework.pagination.Pageable;

/**
 * Simple Dao with basic access methods.
 * @author Benjamin Probst
 *
 * @param <T> Type of the object
 * @param <K> Type of the key
 */
public interface Dao<T, K> {

	/**
	 * Returns a subset of all results based on the {@code Pageable}.
	 * @param pageable Pageable information to be used
	 * @return Subset of all results
	 */
	public abstract Collection<T> findAll(Pageable pageable);

	/**
	 * Updates a existing object.
	 * @param value Existing object
	 */
	public abstract void update(T value);
	
	/**
	 * Inserts a new object.
	 * @param value New object
	 */
	public abstract void insert(T value);
	
	/**
	 * Removes a object based on its key.
	 * @param key The key
	 */
	public abstract void delete(K key);
	
	/**
	 * Finds a object by its key if its present otherwise it will return a {@code Optional#empty()}.
	 * @param key The key
	 * @return Optional that may contain the requestet object
	 */
	public abstract Optional<T> findByKey(K key);
	

	/**
	 * Counts all entries for this type and returns the number.
	 * @return The number of entries
	 */
	public abstract long count();
}
