package codes.probst.framework.dao;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.pagination.Sort;

/**
 * Abstract implementation of the {@code Dao} interface based on the Java Persistance Framework.
 * @author Benjamin Probst
 *
 * @param <T> Type of the object
 * @param <K> Type of the key
 */
public abstract class AbstractJpaDao<T, K> implements Dao<T, K> {
	private EntityManager entityManager;
	private Class<T> jpaClass;
	private SingularAttribute<T, K> keyAttribute;
	
	/**
	 * Constructs a AbstractJpaDao.
	 * @param jpaClass The used {@code Class}
	 * @param keyAttribute The {@code SingularAttribute} used to identify the key
	 */
	public AbstractJpaDao(Class<T> jpaClass, SingularAttribute<T, K> keyAttribute) {
		this.jpaClass = jpaClass;
		this.keyAttribute = keyAttribute;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<T> findAll(Pageable pageable) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<T> criteria = builder.createQuery(jpaClass);
		criteria.from(jpaClass);
		return select(criteria, pageable);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(T value) {
		entityManager.merge(value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void insert(T value) {
		entityManager.persist(value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<T> findByKey(K key) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<T> criteria = builder.createQuery(jpaClass);
		criteria.where(builder.equal(criteria.from(jpaClass).get(keyAttribute), key));
		return selectOnce(criteria);
	}
	
	@Override
	public long count() {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		criteria.select(builder.count(criteria.from(jpaClass)));
		return entityManager.createQuery(criteria).getSingleResult().longValue();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(K key) {
		CriteriaBuilder builder = getCriteriaBuilder();
		CriteriaDelete<T> criteria = builder.createCriteriaDelete(jpaClass);
		Root<T> root = criteria.from(jpaClass);
		criteria.where(builder.equal(root.get(keyAttribute), key));
		delete(criteria);
	}

	/**
	 * Esecutes the given {@code CriteriaUpdate}.
	 * @param criteria The criteria
	 * @return the number of entities updated
	 */
	protected int update(CriteriaUpdate<T> criteria) {
		return entityManager.createQuery(criteria).executeUpdate();
	}
	
	/**
	 * Esecutes the given {@code CriteriaDelete}.
	 * @param criteria The criteria
	 * @return the number of entities deleted
	 */
	protected int delete(CriteriaDelete<T> criteria) {
		return entityManager.createQuery(criteria).executeUpdate();
	}

	/**
	 * Executes the given {@code CriteriaQuery} and retrieves a single element if there is only one, 
	 * {@code Optional#empty()} if there is no result and throws a {@code IllegalStateException}
	 * if there is more then one result.
	 * @param criteria The criteria
	 * @return the {@code Optional} result
	 * @throws IllegalArgumentException if there is more then one result
	 */
	protected Optional<T> selectOnce(CriteriaQuery<T> criteria) {
		TypedQuery<T> query = entityManager.createQuery(criteria);
		List<T> result = query.getResultList();
		
		if(result == null || result.isEmpty()) {
			return Optional.empty();
		} else {
			return Optional.of(result.get(0));
		}
	}
	
	/**
	 * Selects a {@code List} of elements and applies the given {@code Pageable} if available.
	 * @param criteria
	 * @param pageable
	 * @return
	 */
	protected List<T> select(CriteriaQuery<T> criteria, Pageable pageable) {
		if(pageable != null) {
			Sort[] sorts = pageable.getSort();
			if(sorts != null && sorts.length > 0) {
				CriteriaBuilder builder = getCriteriaBuilder();
				Set<Root<?>> roots = criteria.getRoots();
				
				List<Order> orders = Arrays.stream(sorts).map(sort -> {
					Path<T> path = null;
					
					OUTER_LOOP:
					for(Root<?> root : roots) {
						try {
							path = root.get(sort.getName());
							break;
						} catch(IllegalArgumentException e) {
							Set<?> joins = root.getJoins();
							if(joins != null) {
								for(Iterator<?> it = joins.iterator(); it.hasNext(); ) {
									Join<?, ?> join = (Join<?, ?>) it.next();
									try {
										path = join.get(sort.getName());
										break OUTER_LOOP;
									} catch(IllegalArgumentException e2) {
									}
								}
							}
						}
					}
					
					switch(sort.getDirection()) {
						case ASCENDING: return builder.asc(path);
						case DESCENDING: return builder.desc(path);
					}
					return null;
				}).collect(Collectors.toList());
				
				criteria.orderBy(orders);
			}
		}
		TypedQuery<T> query = entityManager.createQuery(criteria);
		
		if(pageable != null) {
			query.setFirstResult(pageable.getCurrentPage() * pageable.getPageSize());
			query.setMaxResults(pageable.getPageSize());
		}
		
		return query.getResultList();
	}
	
	/**
	 * Returns the {@code CriteriaBuilder}.
	 * @return The criteria builder
	 */
	protected CriteriaBuilder getCriteriaBuilder() {
		return entityManager.getCriteriaBuilder();
	}
	
	/**
	 * Sets the {@code EntityManager}.
	 * @param entityManager the entity manager
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	protected EntityManager getEntityManager() {
		return entityManager;
	}
}
