package codes.probst.framework.service;

import java.util.Collection;
import java.util.Optional;

import codes.probst.framework.pagination.Pageable;

/**
 * Basic service that supports basic object access and manipulation.
 * @author Benjamin Probst
 *
 * @param <T> Type of the object
 * @param <K> Type of the key
 */
public interface Service<T, K> {

	/**
	 * Returns the {@code Optional} that may contain the object with the key if its available.
	 * @return An {@code Optional} that may contain the requested object
	 */
	public Optional<T> getByKey(K key);

	/**
	 * Returns a {@code Collection} that represents a subset of the total results specified by the {@code Pageable}. 
	 * @return Subset of all available results
	 */
	public Collection<T> getAll(Pageable pageable);

	/**
	 * Saves a object. If the object is already stored the changes are persisted and if the object is not stored a new entry is persisted.
	 * @param object Object to persist
	 */
	public void save(T object);
	
	
	/**
	 * Removes a object from the persistance based on the key. Does nothing if there is no matching key.
	 * @param key Key to remove
	 */
	public void remove(K key);
}