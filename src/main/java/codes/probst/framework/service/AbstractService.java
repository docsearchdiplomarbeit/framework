package codes.probst.framework.service;

import java.util.Collection;
import java.util.Optional;

import codes.probst.framework.dao.Dao;
import codes.probst.framework.pagination.Pageable;

/**
 * Abstract implementation of a {@code Service} backed by a {@code Dao} that supports all methods of the dao.
 * 
 * @author Benjamin Probst
 *
 * @param <D> Type of the {@code Dao}
 * @param <T> Type of the returned object
 * @param <K> Type of the key
 */
public abstract class AbstractService<D extends Dao<T, K>, T, K> implements Service<T, K> {
	private D dao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<T> getByKey(K id) {
		return getDao().findByKey(id);
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<T> getAll(Pageable pageable) {
		return getDao().findAll(pageable);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void save(T object) {
		if(isNew(object)) {
			dao.insert(object);
		} else {
			dao.update(object);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove(K key) {
		dao.delete(key);
	}
	
	/**
	 * Verifies if a object is already available in the persistance or if its new.
	 * @param object The object to validate
	 * @return {@code true} if the object is new and {@code false} if its already persisted
	 */
	protected abstract boolean isNew(T object);
	
	/**
	 * Returns the {@code Dao} of this service.
	 * @return The dao
	 */
	protected D getDao() {
		return dao;
	}
	
	/**
	 * Sets the {@code Dao} for this service.
	 * @param dao The dao
	 */
	public void setDao(D dao) {
		this.dao = dao;
	}
	
}
