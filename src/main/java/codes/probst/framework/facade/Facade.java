package codes.probst.framework.facade;

import java.util.Collection;
import java.util.Optional;

import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.populator.PopulationOption;

public interface Facade<S, T, K> {

	public Optional<T> getByKey(K key, PopulationOption... options);

	public Collection<T> getAll(Pageable pageable, PopulationOption... options);

}