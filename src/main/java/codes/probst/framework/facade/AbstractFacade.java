package codes.probst.framework.facade;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import codes.probst.framework.converter.Converter;
import codes.probst.framework.pagination.Pageable;
import codes.probst.framework.populator.ConfigurablePopulator;
import codes.probst.framework.populator.PopulationOption;
import codes.probst.framework.service.Service;

/**
 * Abstract facade that provides options to convert and/or populate objects.
 * @author Benjamin Probst
 *
 * @param <S> Type of the source object
 * @param <T> Type of the target object
 */
public abstract class AbstractFacade<S, T, I extends Service<S, K>, K> implements Facade<S, T, K> {
	private Converter<S, T> converter;
	private ConfigurablePopulator<S, T> populator;
	private I service;
	
	@Override
	public Optional<T> getByKey(K key, PopulationOption... options) {
		Optional<S> optional = service.getByKey(key);
		
		if(optional.isPresent()) {
			return Optional.of(convertAndPopulate(optional.get(), options));
		}
		
		return Optional.empty();
	}
	
	@Override
	public Collection<T> getAll(Pageable pageable, PopulationOption... options) {
		Collection<S> result = service.getAll(pageable);
		
		return convertAndPopulateAll(result, options);
	}

	/**
	 * Conterts the source object to the target type and runs the populator based on the given {@code PopulationOption}s.
	 * @param source Source object
	 * @param options Options to populate
	 * @return Target object
	 */
	protected T convertAndPopulate(S source, PopulationOption... options) {
		T target = convert(source);
		populate(source, target, options);
		return target;
	}
	
	/**
	 * Converts a {@code Collection} of source elements to a list of source elements based on the {@code PopulationOption}s.
	 * @param sources {@code Collection} of source elements
	 * @param options Options to populate
	 * @return {@code Collection} of target elements
	 */
	protected Collection<T> convertAndPopulateAll(Collection<S> sources, PopulationOption... options) {
		return sources.stream().map(source -> {
			T target = convert(source);
			populate(source, target, options);
			return target;
		}).collect(Collectors.toList());
	}
	
	/**
	 * Converts a source object to the target object. If the source object is {@code null} the target will also be {@code null}.
	 * @param source Source object
	 * @return Target object
	 */
	protected T convert(S source) {
		if(source == null) {
			return null;
		}
		
		return converter.convert(source);
	}
	
	/**
	 * Populates values based on the source element and requested by the {@code PopulationOption}s to the target object.
	 * @param source Source object
	 * @param target Target object
	 * @param options Options to populate
	 */
	protected void populate(S source, T target, PopulationOption... options) {
		if(source != null) {
			populator.populate(source, target, options);
		}
	}
	
	/**
	 * Sets the converter to be used.
	 * @param converter The converter
	 */
	public void setConverter(Converter<S, T> converter) {
		this.converter = converter;
	}
	
	/**
	 * Sets the populator to be used.
	 * @param populator The populator
	 */
	public void setPopulator(ConfigurablePopulator<S, T> populator) {
		this.populator = populator;
	}
	
	/**
	 * Returns the service for this facade.
	 * @return The service
	 */
	protected I getService() {
		return service;
	}
	
	/**
	 * Sets the service for this facade.
	 * @param service The service
	 */
	public void setService(I service) {
		this.service = service;
	}
}
